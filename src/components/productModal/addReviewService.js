import { toast } from 'react-toastify';
import HttpHelper from '../../utils/HttpHelper';
import Constants from '../../utils/constants';
import CallToastContainer from '../toast/toast';

/**
 * This function will take in the review and send that review into the database
 * @name addReview
 * @description Utilizes HttpHelper to make a post request to an API
 * @param {*} review the review that needs to be pushed into the database
 * @returns boolen
 */
export default async function createReview(review) {
  await HttpHelper(Constants.ALL_REVIEWS_ENDPOINT, 'POST', review)
    .then((response) => {
      response.json();
      if (response.ok) {
        setTimeout(() => {
          CallToastContainer('Successfully Posted!', 6000, true, toast.POSITION.TOP_CENTER, toast.success);
        }, 500);
      }
    })
    .catch(() => {
      CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      return false;
    });
}
