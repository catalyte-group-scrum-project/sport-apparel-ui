/* eslint-disable max-len */
import React, { useState, useRef, useEffect } from 'react';
import Collapse from '@material-ui/core/Collapse';
import { toast } from 'react-toastify';
import { makeStyles, styled } from '@material-ui/core/styles';
import CardMedia from '@material-ui/core/CardMedia';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Rating from '@material-ui/lab/Rating';
import styles from './ModalContainer.module.css';
import { useCart } from '../checkout-page/CartContext';
import CallToastContainer from '../toast/toast';
import AddReviewForm from './ReviewContent';
import ReviewCard from '../product-review-card/ReviewCard';
import { fetchFilteredReviews } from './ReviewsService';
import Constants from '../../utils/constants';
import { getReviewsByEmail } from '../product-page/ProductPageService';

/**
 * @name useStyles
 * @description Material-ui styling for ProductCard component
 * @return styling
 */
const useStyles = makeStyles(() => ({
  media: {
    paddingTop: '56.25%',
    width: '300px'
  }
}));

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <button type="button" {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(0deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest
  })
}));

/**
 * @name ModalContainer
 * @description displays single product card component
 * @param {*} props modalProduct, toggleModal, modalIsOpen, likedProductIds, handelLikeProduct, userProducts, userReviews, userEmail, setUserProductsReviewed
 * @return component
 */
const ModalContainer = ({
  modalProduct, toggleModal, modalIsOpen, likedProductIds, handelLikeProduct, userProducts, userReviews, userEmail, setUserProductsReviews, email
}) => {
  const classes = useStyles();
  const { dispatch } = useCart();
  const modalRef = useRef(null);
  const [qty, setQty] = useState(1);
  const [createReviewForm, setCreateReviewForm] = useState(false);

  const qtyDecr = () => {
    if (qty > 0) {
      setQty(qty - 1);
    }
  };
  const qtyIncr = () => {
    setQty(qty + 1);
  };

  const trySetQty = (e) => {
    const num = e.target.qty;
    if (Number.isInteger(num)) {
      setQty(num);
    }
  };
  useEffect(() => {
    if (modalIsOpen) modalRef.current.focus();
  });

  const checkKeys = (e) => {
    if (e.key === 'Escape') toggleModal();
  };

  const onAdd = () => {
    dispatch(
      {
        type: 'add',
        product: {
          id: modalProduct.id,
          title: modalProduct.name,
          price: modalProduct.price,
          description: modalProduct.description,
          quantity: qty,
          imageSrc: modalProduct.imageSrc
        }
      }
    );

    CallToastContainer(`Success! ${qty} ${modalProduct.name} added to cart`, 8000, true, toast.POSITION.TOP_CENTER, toast.success);
  };
  /* console.log(`3 ${(!userReviews.includes(modalProduct.id))} id ${modalProduct.id} uR ${userReviews}`); */
  /**
   * function that will handel the Post to wishlist request and provide some simple validations
   * to prevent duplicate products
   */

  const [expanded, setExpanded] = useState(false);
  const [apiError, setApiError] = useState(false);
  const [filteredReviews, setFilteredReviews] = useState([]);
  const [newReview, setNewReview] = useState([false]);
  const [order, setOrder] = useState(true);

  const sorting = (date) => {
    if (order === true) {
      const sorted = ([...filteredReviews].sort((a, b) => (a[date] > b[date] ? 1 : -1)));
      setFilteredReviews(sorted);
      setOrder(false);
    }
    if (order === false) {
      const sorted = ([...filteredReviews].sort((a, b) => (a[date] < b[date] ? 1 : -1)));
      setFilteredReviews(sorted);
      setOrder(true);
    }
  };

  useEffect(() => {
    fetchFilteredReviews(setFilteredReviews, setApiError, modalProduct.id);
    getReviewsByEmail(setUserProductsReviews, email);
    setNewReview(false);
  }, [modalProduct.id, newReview, setUserProductsReviews, email]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const toggleForm = () => {
    if (!expanded && !createReviewForm) { /* if both are closed */
      handleExpandClick(); /* open reviews */
    }
    setCreateReviewForm(!createReviewForm);
  };
  return (
    <div
      className={styles.productModalBackground}
      onClick={toggleModal}
      role="button"
      tabIndex={0}
      onKeyDown={checkKeys}
    >
      {apiError && <p className={styles.errMsg} data-testid="errMsg">{Constants.API_ERROR}</p>}
      <CallToastContainer />
      <div
        className={(expanded || createReviewForm) ? styles.productModalContainerReviewsOpened : styles.productModalContainerReviewsClosed}
        onClick={(e) => e.stopPropagation()}
        role="button"
        tabIndex={0}
        onKeyDown={checkKeys}
        ref={modalRef}
      >
        <div
          className={styles.header100}
          title={modalProduct.name}
          align="center"
        >
          <h3 className={styles.header100}>
            {modalProduct.name}
          </h3>
          <div>
            <button className={styles.closeModal} type="button" onClick={toggleModal}>X</button>
          </div>
        </div>
        <Box
          className={styles.imageBox}
        >
          <div className={styles.imageSpacer} />
          <Box
            className={styles.imageBorder}
          >
            <CardMedia
              title="placeholder"
              image={modalProduct.imageSrc}
              className={classes.media}
            />
          </Box>
          <div className={styles.imageSpacer} />
        </Box>
        <Box
          className={styles.descOuterBox}
        >
          <div className={styles.descSpacer} />
          <Box
            className={styles.descInnerBox}
          >
            <p
              variant="body2"
              color="textSecondary"
              component="p"
              className={styles.descText}
            >
              {modalProduct.description}
            </p>
          </Box>
          <div className={styles.descSpacer} />
        </Box>
        <div className={styles.priceDiv}>
          <Typography variant="body2" component="p" align="left">
            Price: $
            {modalProduct.price.toFixed(2)}
          </Typography>
        </div>
        <div className={styles.swatchDiv}>
          <Box
            id="box1"
            className={styles.colorSwatch}
            sx={{
              backgroundColor: modalProduct.primaryColorCode
            }}
          />
          <div className={styles.swatchSeparator} />
          <Box
            id="box2"
            className={styles.colorSwatch}
            sx={{
              backgroundColor: modalProduct.secondaryColorCode
            }}
          />
        </div>

        <div className={styles.buttonRow}>
          <IconButton
            onClick={() => { handelLikeProduct(modalProduct); }}
            aria-label="add to favorites"
            className={styles.buttonFav}
            padding="0"
          >
            {likedProductIds.includes(modalProduct.id)
              ? <FavoriteIcon className={styles.red} />
              : <FavoriteIcon /> }
          </IconButton>
          <div className={styles.buttonRating}>
            <Rating
              name="read-only"
              value={null}
              readOnly
              className={styles.rating}
            />
          </div>
          <div className={styles.buttonQty}>
            <div className={styles.spinnerDiv}>
              <button type="button" onClick={() => qtyDecr()}>-</button>
              <input
                type="text"
                value={qty}
                defaultValue="1"
                onChange={trySetQty}
                className={styles.spinnerInput}
              />
              <button type="button" onClick={() => qtyIncr()}>+</button>
            </div>
          </div>
          <div className={styles.buttonCart}>
            <IconButton aria-label="add to shopping cart" onClick={onAdd}>
              <AddShoppingCartIcon />
            </IconButton>
          </div>
          <div className={styles.buttonRev}>
            <button className={styles.buyNowButton} type="button">Buy Now</button>
          </div>
        </div>
        <div className={styles.reviewSubmitButtonRow}>
          <div className={styles.reviewViewDiv} hidden={filteredReviews.length === 0}>
            <ExpandMore
              expand={expanded}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show reviews"
              className={styles.reviewViewButton}
              hidden={filteredReviews.length === 0}
            >
              View Reviews
            </ExpandMore>
          </div>
          { (userProducts !== null) && (filteredReviews.length !== 0)
          && (userProducts.includes(modalProduct.id)
          && ((userReviews === null) || (!userReviews.includes(modalProduct.id))))
            ? (<div className={styles.swatchSeparator} />)
            : (<div className={styles.reviewNoSeparator} />)}
          { (userProducts !== null)
          && (userProducts.includes(modalProduct.id)
          && ((userReviews === null) || (!userReviews.includes(modalProduct.id))))
            ? (<button type="button" className={styles.reviewButton} onClick={toggleForm}>Create Review</button>)
            : (<div />)}
        </div>
        {createReviewForm
          ? (
            <div className={styles.reviewFormContainer}>
              <AddReviewForm
                className={styles.reviewFormContainer}
                toggleForm={toggleForm}
                product={modalProduct}
                userEmail={userEmail}
                setNewReview={setNewReview}
                setUserProductsReviews={setUserProductsReviews}
                userReviews={userReviews}
              />
            </div>
          )
          : <div />}
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <div>
            <div>
              <button type="button" onClick={() => sorting('dateCreated')} className={styles.sortButton}>
                Sort by date
              </button>
            </div>
            <div className={styles.app}>
              {filteredReviews.map((review) => (
                <div key={review.id}>
                  <ReviewCard
                    review={review}
                    userEmail={userEmail}
                  />
                </div>
              ))}
            </div>
          </div>
        </Collapse>
      </div>
    </div>
  );
};

export default ModalContainer;
