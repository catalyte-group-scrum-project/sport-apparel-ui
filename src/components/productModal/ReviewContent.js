import React, { useState } from 'react';
import { toast } from 'react-toastify';
import Rating from '@material-ui/lab/Rating';
import styles from './ModalContainer.module.css';
import createReview from './addReviewService';
import CallToastContainer from '../toast/toast';

/**
 * @name ReviewComment
 * @description Review Comment Input field
 * @params value, id, placeholder, type, errorMsg, inputStyle, maxLength, onChange
 * @return component
 */
const ReviewComment = ({
  value, id, placeholder, type, errorMsg, inputStyle, maxLength, onChange
}) => (

  <div className={styles.reviewCommentInputBox}>
    <p className={styles.reviewErrorMsg}>{errorMsg}</p>
    <textarea
      style={inputStyle}
      className={styles.reviewCommentTextArea}
      id={id}
      placeholder={placeholder}
      type={type}
      value={value}
      maxLength={maxLength}
      onChange={onChange}
    />
  </div>
);
/**
 * @name ReviewTitle
 * @description Review Title Input field
 * @params value, id, placeholder, type, errorMsg, inputStyle, maxLength, onChange
 * @return component
 */
const ReviewTitle = ({
  value, id, placeholder, type, errorMsg, inputStyle, maxLength, onChange
}) => (
  <div className={styles.reviewTitleBox}>
    <div className={styles.reviewErrorMsg}>
      {errorMsg}
      <br />
      <input
        style={inputStyle}
        className={styles.reviewTitleInput}
        id={id}
        placeholder={placeholder}
        type={type}
        value={value}
        maxLength={maxLength}
        onChange={onChange}
      />
    </div>
  </div>
);

/**
 * @name AddReviewForm
 * @description form that allows a review to be added
 * @params toggleForm, product, userEmail, setNewReview
 * @return component
 */
const AddReviewForm = ({
  toggleForm, product, userEmail, setNewReview
}) => {
  const [reviewErrors, setReviewErrors] = useState({});
  const [reviewData, setReviewData] = useState({});
  const [rating, setRating] = useState(0);
  const onReviewDataChange = (e) => {
    setReviewData({ ...reviewData, [e.target.id]: e.target.value });
  };
  const whitespaceOnlyReg = /^\s*$/;
  // eslint-disable-next-line max-len
  const checkIfWhitespace = (s) => whitespaceOnlyReg.test(s) || s === undefined;
  const checkCancelReview = (e) => {
    const review = {
      comment: reviewData.comment,
      title: reviewData.title
    };
    /* if no comment was input */
    if (checkIfWhitespace(review.comment) && (checkIfWhitespace(review.title))) {
      setReviewErrors({ /* clear errors */
        comment: '',
        title: '',
        rating: ''
      });
      toggleForm(); /* close the form */
      e.preventDefault(); /* don't submit */
    } else if (window.confirm('Are you sure you wish to abandon your changes?')) {
      /* if user has entered something and agrees they wish to abandon changes */
      toggleForm();
      e.preventDefault(); /* don't submit */
    }
  };
  /* user pressed submit, now check errors and send if there are no errors */
  const checkReviewErrors = (e) => {
    const ratingInt = parseInt(rating, 10);
    let commentErr = '';
    let ratingErr = '';
    const review = {
      comment: reviewData.comment,
      rating: ratingInt,
      title: reviewData.title,
      productID: product.id,
      email: userEmail
    };
    if (checkIfWhitespace(review.comment)) { /* if no comment was input */
      commentErr = 'review must have comment to submit';
      e.preventDefault(); /* don't submit */
    }
    if (review.rating <= 0) {
      ratingErr = 'rating must be greater or equal to 1';
      e.preventDefault(); /* don't submit */
    }
    if ((!commentErr) && (!ratingErr)) { /* if there are no errors */
      setReviewErrors({ /* reset errors */
        comment: '',
        title: '',
        rating: ''
      });
      createReview(review); /* send the review to the database */
      /* createReview will post a success toast if the review is added */
      setNewReview(true);
      toggleForm(); /* close the form */
    } else {
      setReviewErrors({
        comment: commentErr,
        rating: ratingErr,
        title: ''
      });
      CallToastContainer('Error! Fix errors before review can be added', 8000, true, toast.POSITION.TOP_CENTER, toast.error);
    }
  };
  const invalidInputCss = {
    border: '2px solid red'
  };
  const validInputCss = {
    border: '1px solid black'
  };

  return (
    <div className={styles.reviewFormOuterBox}>
      <div className={styles.buttonRow}>
        <div className={styles.reviewCreateReviewDiv}>
          <h3>Create Review</h3>
        </div>
      </div>
      <div className={styles.reviewRatingRow}>
        <div className={
            reviewErrors.rating ? styles.reviewRatingBoxInvalid : styles.reviewRatingBoxValid
            }
        >
          <p className={styles.reviewErrorMsg}>{reviewErrors.rating}</p>
          <div className={styles.reviewRatingStars}>
            <Rating
              name="simple-controlled"
              value={rating}
              className={styles.rating}
              onChange={(event, newRating) => setRating(newRating)}
              precision={1.0}
              size="large"
              defaultValue={0}
            />
          </div>
          <div className={styles.reviewRatingPDiv}>
            <p className={styles.reviewRatingP}>Enter a rating</p>
          </div>
        </div>
      </div>
      <div className={styles.reviewTitleRow}>
        <ReviewTitle
          className={styles.reviewTitle}
          placeholder="give your review a title"
          type="text"
          id="title"
          onChange={onReviewDataChange}
          value={reviewData.title}
          errorMsg={reviewErrors.title}
          inputStyle={validInputCss}
          maxLength="100"
        />
      </div>
      <div className={styles.reviewCommentRow}>
        <ReviewComment
          className={styles.reviewCommentIputBox}
          placeholder="type your review here, up to 1000 characters"
          type="text"
          id="comment"
          onChange={onReviewDataChange}
          value={reviewData.comment}
          errorMsg={reviewErrors.comment}
          inputStyle={reviewErrors.comment ? invalidInputCss : validInputCss}
          maxLength="1000"
        />
      </div>
      <div className={styles.reviewSubmitButtonRow}>
        <div className={styles.reviewSubmitDiv}>
          <button type="submit" onClick={checkReviewErrors} className={styles.submitOrCancelButton}>submit</button>
        </div>
        <div className={styles.swatchSeparator} />
        <div className={styles.reviewSubmitDiv}>
          <button type="button" onClick={checkCancelReview} className={styles.submitOrCancelButton}>cancel</button>
        </div>
      </div>

    </div>
  );
};

export default AddReviewForm;
