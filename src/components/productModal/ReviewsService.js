import HttpHelper from '../../utils/HttpHelper';
import Constants from '../../utils/constants';

/**
*
* @name fetchFilteredReviews
* @description Utilizes HttpHelper to make a get request to an API
* @param {*} setFilteredReviews sets state for reviews
* @param {*} setApiError sets error if response other than 200 is returned
* @param {*} productId product Id that has reviews to view
* @returns sets state for products if 200 response, else sets state for apiError
*/
export async function fetchFilteredReviews(setFilteredReviews, setApiError, productId) {
  await HttpHelper(`/reviews/productId/${productId}`, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setFilteredReviews)
    .catch(() => {
      setApiError(true);
    });
}

/**
*
* @name fetchUserByEmail
* @description Utilizes HttpHelper to make a get request to an API
* @param {*} setUser sets state for user
* @param {*} setApiError sets error if response other than 200 is returned
* @param {*} userEmail user email that is attached to review
* @returns sets state for products if 200 response, else sets state for apiError
*/
export async function fetchUserByEmail(setUser, setApiError, userEmail) {
  await HttpHelper(`/users/${userEmail}`, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setUser)
    .catch(() => {
      setApiError(true);
    });
}
