// import React, { useState } from 'react';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { toast } from 'react-toastify';
import CallToastContainer from '../toast/toast';
import styles from './ProductCard.module.css';
import { useCart } from '../checkout-page/CartContext';
import { updateLatestView, updateProductViews } from '../product-page/ProductPageService';

/**
 * @name useStyles
 * @description Material-ui styling for ProductCard component
 * @return styling
 */
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    maxWidth: 345
  },
  rootBlur: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    maxWidth: 345,
    filter: 'blur(2px)'
  },

  media: {
    height: 0,
    paddingTop: '56.25%'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: red[500]
  },
  header: {
    height: '100%'
  }
}));

/**
 * @name ProductCard
 * @description displays single product card component
 * @param {*} props product
 * @return component
 */
const ProductCard = ({
  product, toggleModal, modalIsOpen, likedProductIds, handelLikeProduct
}) => {
  const classes = useStyles();

  const { dispatch } = useCart();

  let currentTime = 0;

  const handelTime = () => {
    currentTime = new Date().toUTCString();
    return currentTime;
  };

  const onAdd = () => {
    CallToastContainer(`${'Success!'} ${product.name} ${'has been added to cart.'}`, 6000, false, toast.POSITION.TOP_CENTER, toast.success);
    const productTitleLegnthCheck = () => {
      if (product.name.length > 31) {
        return (`${product.name.slice(0, 30)}${'...'}`);
      }
      return product.name;
    };
    dispatch(
      {
        type: 'add',
        product: {
          id: product.id,
          title: productTitleLegnthCheck(),
          price: product.price,
          description: product.description,
          quantity: 1,
          imageSrc: product.imageSrc
        }
      }
    );
    handelTime();
    if (product.latestView !== currentTime && (product.latestView + 60000) <= currentTime) {
      updateProductViews(product.id);
      updateLatestView(product.id);
    }
  };
  return (
    <Card className={modalIsOpen ? classes.rootBlur : classes.root}>
      <CallToastContainer />
      <CardHeader
        onClick={() => toggleModal({ product })}
        className={classes.header}
        avatar={(
          <Avatar aria-label="demographics" className={classes.avatar}>
            {product.demographic.charAt(0)}
          </Avatar>
        )}
        action={(
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>

        )}
        title={product.name}
        subheader={`${product.demographic} ${product.category} ${product.type}`}
      />
      <CardMedia
        onClick={() => toggleModal({ product })}
        className={classes.media}
        image={product.imageSrc}
        title="placeholder"
      />
      <CardContent onClick={() => toggleModal({ product })}>
        <Typography variant="body2" color="textSecondary" component="p">
          {product.description}
        </Typography>
        <br />
        <Typography variant="body2" color="textSecondary" component="p">
          Price: $
          {product.price.toFixed(2)}
        </Typography>
      </CardContent>
      <CardActions disableSpacing className={classes.bonttomContent}>
        {likedProductIds
          ? (
            <IconButton aria-label="add to favorites" onClick={() => { handelLikeProduct(product); }}>
              {likedProductIds.includes(product.id)
                ? <FavoriteIcon className={styles.red} />
                : <FavoriteIcon className={styles.grey} />}
            </IconButton>
          ) : null}
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton aria-label="add to shopping cart" onClick={onAdd} className={styles.ripple}>
          <AddShoppingCartIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default ProductCard;
