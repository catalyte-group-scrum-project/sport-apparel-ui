import { React, useState } from 'react';
import './App.css';
import {
  BrowserRouter, Route, Switch, Redirect
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import ProductPage from '../product-page/ProductPage';
import CheckoutPage from '../checkout-page/CheckoutPage';
import ConfirmationPage from '../confirmation-page/ConfirmationPage';
import MaintenancePage from '../maintenance-page/MaintenancePage';
<<<<<<< HEAD
=======
import CreateProductPage from '../maintenance-page/create-product-page/CreateProductPage';
<<<<<<< HEAD
>>>>>>> 3cd3349f19af760ce835af4091b8a05c448b6af1
=======
import HomePage from '../home-page/home-page';
>>>>>>> op-33-view-product-reviews
import Header from '../header/Header';
import Footer from '../Footer/Footer';
import AccountPage from '../account-page/AccountPage';

/**
 * @name App
 * @description when user is not logged in then profile is not created
 * @description if user logged in then logged out in profile page then Redirect to "/"
 * @description clicking profile icon will open profile when logged in
 * @returns component
 */
<<<<<<< HEAD
const App = () => (
<<<<<<< HEAD
  <div className="container">
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" render={() => <ProductPage />} />
        <Route exact path="/checkout" render={() => <CheckoutPage />} />
        <Route exact path="/confirmation" render={() => <ConfirmationPage />} />
        <Route exact path="/maintenance" render={() => <MaintenancePage />} />
      </Switch>
=======
  <div className="app">
    <BrowserRouter>
      <Header />
      <div className="pages">
        <Switch>
          <Route exact path="/" render={() => <ProductPage />} />
          <Route exact path="/checkout" render={() => <CheckoutPage />} />
          <Route exact path="/confirmation" render={() => <ConfirmationPage />} />
          <Route exact path="/maintenance" render={() => <MaintenancePage />} />
          <Route exact path="/maintenance/create-product" render={() => <CreateProductPage />} />
        </Switch>
      </div>
>>>>>>> 3cd3349f19af760ce835af4091b8a05c448b6af1
    </BrowserRouter>
    <ToastContainer toastStyle={{ width: '200px' }} theme="colored" />
    <Footer />
  </div>
);
=======
const App = () => {
  const [loginstatus, setLoginStatus] = useState(null);
  return (
    <div className="app">
      <BrowserRouter>
        <Header setLoginStatus={setLoginStatus} />
        {loginstatus === false && (window.location.href.split(window.location.host)[1] === '/account') ? <Redirect to="/home" /> : null}
        <div className="pages">
          <Switch>
            <Route exact path="/" render={() => <HomePage />} />
            <Route exact path="/home" render={() => <HomePage />} />
            {(loginstatus === true)
              ? <Route exact path="/products" render={() => <ProductPage loginStatus={loginstatus} />} />
              : <Route exact path="/products" render={() => <ProductPage loginStatus={false} />} />}
            <Route exact path="/checkout" render={() => <CheckoutPage />} />
            <Route exact path="/confirmation" render={() => <ConfirmationPage />} />
            <Route exact path="/maintenance" render={() => <MaintenancePage />} />
            <Route exact path="/maintenance/create-product" render={() => <CreateProductPage />} />
            <Redirect exact from="/account" to="/account/profile" />
            {loginstatus === true ? <Route exact path="/account/:page?" render={(props) => <AccountPage history={props.history} page={props.match.params.page} />} /> : null }
          </Switch>
        </div>
      </BrowserRouter>
      <ToastContainer toastStyle={{ width: '200px' }} theme="colored" />
      <Footer />
    </div>
  );
};
>>>>>>> 5cee39ccf227d7e9ef6f94ab9b26b79dcf5b9dc2
export default App;
