import React, { useEffect, useState, useRef } from 'react';
import { NavLink } from 'react-router-dom';
import GoogleLogin, { GoogleLogout } from 'react-google-login';
import { MdOutlineShoppingCart } from 'react-icons/md';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Avatar from '@material-ui/core/Avatar';
import { Badge } from '@material-ui/core';
import loginUser, { updateLastActiveTime } from './HeaderService';
import constants from '../../utils/constants';
import logo from '../assets/uiLogo.jpg';
import styles from './Header.module.css';
import { useCart } from '../checkout-page/CartContext';
import SearchBarToggle from '../siteSearch/SearchBarToggle';
import SearchByClick from '../siteSearch/SearchByClick';
import SearchByEnter from '../siteSearch/SearchByEnter';
import useOutsideClickHandler from '../siteSearch/useOutsideClickHandler';
import { getLikedProductIds } from '../account-page/wishlist/WishListService';

/**
 * @name Header
 * @description Displays the navigation header
 * @return component
 */

const Header = ({ setLoginStatus }) => {
  const { state: { products, areProductsLiked } } = useCart();
  const [likedProductIds, setLikedProductIds] = useState([]);
  const [user, setUser] = useState('');
  const [googleError, setGoogleError] = useState('');
  const [apiError, setApiError] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const searchRef = useRef(null);
  useOutsideClickHandler(searchRef, () => { if (isActive) setIsActive(true); });
  const { dispatch } = useCart();
  const displayNoLikeButton = () => {
    dispatch(
      {
        type: 'toggle-disliked'
      }
    );
  };
  /**
   * @name handleGoogleLoginSuccess
   * @description Function to run if google login was successful
   * @param {Object} response Response object from google
   */
  const handleGoogleLoginSuccess = (response) => {
    sessionStorage.setItem('token', response.getAuthResponse().id_token);
    const googleUser = {
      email: response.profileObj.email,
      firstName: response.profileObj.givenName,
      lastName: response.profileObj.familyName
    };
    loginUser(googleUser, setUser, setApiError);
    setGoogleError('');
    setLoginStatus(true);
    updateLastActiveTime();
    sessionStorage.setItem('loggedIn', 'true');
  };

  /**
   * @name handleGoogleLoginSuccess
   * @description Function to run if google login was unsuccessful
   */
  const handleGoogleLoginFailure = () => {
    setGoogleError('There was a problem logging in with Google. Please wait and try again later.');
    setUser('');
    setGoogleError('');
  };

  /**
   * @name handleGoogleLogoutSuccess
   * @description Function to run if google logout was successful
   */
  const handleGoogleLogoutSuccess = () => {
    displayNoLikeButton();
    setUser('');
    setGoogleError('');
    setLoginStatus(false);
    updateLastActiveTime();
    sessionStorage.setItem('token', '');
    sessionStorage.setItem('loggedIn', 'false');
  };

  const isLoggedIn = sessionStorage.getItem('loggedIn');

  useEffect(() => {
    if (isLoggedIn) {
      getLikedProductIds(setLikedProductIds);
    }
  }, [isLoggedIn]);
  /**
   * @name handleGoogleLogoutFailure
   * @description Function to run if google logout was unsuccessful
   */
  const handleGoogleLogoutFailure = () => {
    setGoogleError('There was a problem logging out with Google. Please wait and try again later.');
  };

  return (
    <div className={styles.header}>
      <div className={styles.logoverticalcenter}>
        <NavLink to="/home" className="home-button"><a href="/home" className="logo"><img className="logo" src={logo} alt="logo" /></a></NavLink>
      </div>
      {!isActive && (
        <div className={styles.searchBarToggle}>
          <SearchBarToggle setIsActive={setIsActive} />
        </div>
      )}
      <div className={styles.searchContainer} ref={searchRef}>
        {isActive && (
          <div className={styles.searchBar}>
            <SearchByEnter setIsActive={setIsActive} />
          </div>
        )}
        {isActive && (
          <div className={styles.searchSend}>
            <SearchByClick setIsActive={setIsActive} />
          </div>
        )}
      </div>
      <div className={styles.googlelogin}>
        <div className={styles.googleloginverticalcenter}>
          <NavLink to="/account" className={styles.profileIcon}>
            {user && <Avatar aria-label="demographics" className={styles.profileColor}>{user.firstName.charAt(0) + user.lastName.charAt(0)}</Avatar>}
          </NavLink>
          {googleError && <span>{googleError}</span>}
          {apiError && <span>Api Error</span>}
          {!user ? (
            <GoogleLogin
              clientId={constants.GOOGLE_CLIENT_ID}
              buttonText="Login"
              onSuccess={handleGoogleLoginSuccess}
              onFailure={handleGoogleLoginFailure}
              cookiePolicy="single_host_origin"
              isSignedIn
              prompt="select_account"
            />
          ) : (
            <GoogleLogout
              clientId={constants.GOOGLE_CLIENT_ID}
              buttonText="Logout"
              onLogoutSuccess={handleGoogleLogoutSuccess}
              onFailure={handleGoogleLogoutFailure}
              isSignedIn={false}
            />
          )}
        </div>
        <div className={styles.cartverticalcenter}>
          <Badge badgeContent={products.length} color="primary" max={10}>
            <NavLink to="/checkout"><MdOutlineShoppingCart MdCode size="2em" color="cornflowerblue" /></NavLink>
          </Badge>
        </div>
        <div className={styles.favoriteverticlecenter}>
          {!areProductsLiked && (!likedProductIds.length > 0 || !user) ? (
            null
          ) : <NavLink to="/account/wishlist"><FavoriteIcon fontSize="large" className={styles.red} /></NavLink> }
        </div>
      </div>
    </div>
  );
};

export default Header;
