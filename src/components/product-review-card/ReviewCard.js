import React, { useState, useEffect } from 'react';
import { makeStyles, styled } from '@material-ui/core/styles';
import { MdOutlineModeEditOutline } from 'react-icons/md';
import { red } from '@material-ui/core/colors';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Avatar from '@material-ui/core/Avatar';
// import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Rating from '@material-ui/lab/Rating';
import { fetchUserByEmail } from '../productModal/ReviewsService';
import styles from './ReviewCard.module.css';
import Constants from '../../utils/constants';

/**
 * @name useStyles
 * @description Material-ui styling for ReviewCard component
 * @return styling
 */
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    maxWidth: 250
  },

  media: {
    height: 0,
    paddingTop: '56.25%'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  avatar: {
    backgroundColor: red[500],
    height: theme.spacing(4),
    width: theme.spacing(4),
    fontSize: 'small',
    marginLeft: theme.spacing(1)
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  Card: {
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 370,
    width: '370px'
  },
  header: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 370,
    width: '370px'
  },
  CardContent: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 370,
    width: '370px'
  }
}));

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest
  })
}));

/**
 * @name ReviewCard
 * @description displays single review card component
 * @param {*} props review
 * @param {*} props userEmail
 * @return component
 */
const ReviewCard = ({ review, userEmail }) => {
  const classes = useStyles();
  const [user, setUser] = useState('');
  const [expanded, setExpanded] = useState(false);
  const [apiError, setApiError] = useState(false);
  const dateCreated = review.dateCreated.toString();
  const nowDate = new Date(dateCreated);
  const UsFormatter = new Intl.DateTimeFormat('en-US');
  console.log(UsFormatter.format(nowDate));

  useEffect(() => {
    fetchUserByEmail(setUser, setApiError, review.email);
  }, [review.email]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card>
      {apiError && <p className={styles.errMsg} data-testid="errMsg">{Constants.API_ERROR}</p>}
      <CardHeader
        className={classes.header}
        action={(
          <div className={styles.reviewHeader}>
            <div hidden={userEmail !== review.email}>
              <IconButton>
                <MdOutlineModeEditOutline fontSize="medium" />
              </IconButton>

              <IconButton>
                <DeleteOutlineIcon fontSize="small" />
              </IconButton>
            </div>
            <ExpandMore
              expand={expanded}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </ExpandMore>
          </div>
        )}
        title={(
          <div className={styles.reviewHeader}>
            {user.firstName}
            {' '}
            {user.lastName}
            {user && (
              <div>
                <Avatar aria-label="user" className={classes.avatar}>
                  {user.firstName.charAt(0) + user.lastName.charAt(0)}
                </Avatar>
              </div>
            )}
          </div>
        )}
        subheader={(
          <div>
            <div className={styles.buttonRating}>
              <Rating name="read-only" value={review.rating} readOnly className={styles.rating} />
            </div>
            <div>
              {review.title}
            </div>
            <div>
              {UsFormatter.format(nowDate)}
            </div>
          </div>
        )}
      />
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            {review.comment}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default ReviewCard;
