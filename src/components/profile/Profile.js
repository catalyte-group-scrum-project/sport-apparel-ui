import React, { useState } from 'react';
import EditProfileInline from './EditProfileInline';
import CallToastContainer from '../toast/toast';
import editUser from './EditProfileService';
import styles from './Profile.module.css';
import CancelEditProfileModal from './CancelEditProfileModal';
/**
 * @name Profile
 * @description user profile page
 * @returns profile
 */

const Profile = ({ user, toggleEffect, setToggleEffect }) => {
  const [profileData, setProfileData] = useState({});
  const [editProfileInfo, setEditProfileInfo] = useState(false);
  const [showCancelEditModal, setShowCancelEditModal] = useState(false);

  const onProfileChange = (e) => {
    setProfileData({ ...profileData, [e.target.id]: e.target.value });
  };

  const toggleEditInfo = () => {
    setEditProfileInfo(!editProfileInfo);
    setShowCancelEditModal(false);
    setProfileData({ ...user });
  };

  const toggleCancelEditModal = () => {
    setShowCancelEditModal(!showCancelEditModal);
  };

  const saveProfileInfo = () => {
    setEditProfileInfo(!editProfileInfo);
    editUser(profileData);
    setToggleEffect(!toggleEffect);
  };

  const getInputLength = (string) => {
    if (string.length > 2) {
      return string.length - 3;
    }
    return 1;
  };

  return (
    <div>
      <CallToastContainer />
      {!editProfileInfo ? (
        <div className={styles.profile}>
          <div className={styles.userBox}>
            <input
              disabled
              size={user.firstName ? getInputLength(user.firstName) : 1} //eslint-disable-line
              className={styles.nameDisplay}
              value={user.firstName !== null && user.firstName !== undefined ? user.firstName : 'User Name'}
            />
            <input
              disabled
              size={user.lastName}
              className={styles.lastNameDisplay}
              value={user.lastName !== null && user.lastName !== undefined ? user.lastName : 'User Name'}
            />
            <br />
            <h3>{`Email: ${user.email !== null && user.email !== undefined ? user.email : ''}`}</h3>
            <div>
              <h3>Shipping Info:</h3>
              <h4>
                Address:
                <input
                  disabled
                  className={styles.profileDisplay}
                  value={`${user.streetAddress !== null && user.streetAddress !== undefined ? user.streetAddress : ' '}`}
                />
              </h4>
              <h4 style={{ display: user.streetAddress2 ? 'block' : 'none' }}>
                Address 2:
                <input
                  disabled
                  className={styles.profileDisplay}
                  value={`${user.streetAddress2}`}
                />
              </h4>
              <div>
                <h4>
                  City:
                  <input
                    disabled
                    className={styles.profileDisplay}
                    value={`${user.city !== null && user.city !== undefined ? user.city : ' '}`}
                  />
                </h4>
              </div>
              <h4>
                State:
                <input
                  disabled
                  className={styles.profileDisplay}
                  value={`${user.state !== null && user.state !== undefined ? user.state : ' '}`}
                />
              </h4>
              <h4>
                Zip:
                <input
                  disabled
                  className={styles.profileDisplay}
                  value={`${user.zipCode !== null && user.zipCode !== undefined ? user.zipCode : ' '}`}
                />
              </h4>
              <h4>
                Phone:
                <input
                  disabled
                  className={styles.profileDisplay}
                  value={`${user.phoneNumber !== null && user.phoneNumber !== undefined ? user.phoneNumber : ' '}`}
                />
              </h4>
            </div>
          </div>
          <button type="button" className={styles.edit} onClick={toggleEditInfo}>Edit</button>
        </div>
      )
        : (
          <div className={styles.profile}>
            {showCancelEditModal
              ? (
                <CancelEditProfileModal
                  toggleCancelEditModal={toggleCancelEditModal}
                  toggleEditInfo={toggleEditInfo}
                />
              )
              : <div />}
            <EditProfileInline onChange={onProfileChange} profileData={profileData} user={user} />
            <button type="button" className={styles.backButton} onClick={toggleCancelEditModal}>Back</button>
            <button type="button" className={styles.edit} onClick={saveProfileInfo}>Save</button>
          </div>
        )}
    </div>
  );
};
export default Profile;
