import { toast } from 'react-toastify';
import HttpHelper from '../../utils/HttpHelper';
import CallToastContainer from '../toast/toast';
/**
 * update the user obj in the db
 * @param {obj} user the obj that will be converted into JSON and pushed into the db
 * in place of the old obj
 */
export default async function editUser(user) {
  await HttpHelper(`/users/${user.id}`, 'PUT', user)
    .then((response) => response.json())
    .catch(() => {
      CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      return false;
    });
}
