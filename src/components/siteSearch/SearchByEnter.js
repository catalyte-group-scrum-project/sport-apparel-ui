import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import InputBase from '@material-ui/core/InputBase';
import styles from './SearchResult.module.css';
import { ProductContext } from '../product-page/ProductContext';

/**
 * @name HandleKeyPress
 * @description used to allow the user to press enter to send their search.
 * funtions similairly to SearchByClick.
 */
const HandleKeyPress = ({
  e, setIsActive, history, setCurrentPage, searchedTerm
}) => {
  if (e.key === 'Enter') {
    if (searchedTerm !== '') {
      history.push('/products');
    }
    setIsActive(false);
    setCurrentPage(1);
  }
};
/**
 * @name SearchByEnter
 * @description used to display the search bar and retrieve the user's input
 * @param {('')} setSearchTerm set the search term to send from user input
 * @returns search bar with input field and sets the search term
 */
const SearchByEnter = ({ setIsActive }) => {
  const { setSearchedTerm, searchedTerm, setCurrentPage } = useContext(ProductContext);
  const history = useHistory();
  return (
    <div className={styles.searchBarContainer}>
      <InputBase
        id="searchBarEntry"
        placeholder="Search..."
        autoComplete="off"
        value={searchedTerm}
        onChange={(e) => {
          setSearchedTerm(e.target.value);
        }}
        onKeyPress={(e) => {
          HandleKeyPress({
            e,
            setIsActive,
            history,
            searchedTerm,
            setSearchedTerm,
            setCurrentPage
          });
        }}
      />
    </div>
  );
};

export default SearchByEnter;
