import { useEffect } from 'react';

/**
 * @name ProductModal
 * @description a react hook that handles click events outside of the referenced element
 * @param {} ref: the referenced element
 * @param {function} callback: a callback function that handles the outsideClick event
 */
export default function useOutsideClickHandler(ref, callback) {
  useEffect(() => {
    const handleClickOutside = (e) => {
      if (ref.current && !ref.current.contains(e.target)) {
        callback();
      }
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });
}
