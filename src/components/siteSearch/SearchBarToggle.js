import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import styles from './SearchResult.module.css';

/**
 * @name SearchBarToggle
 * @description used to open the search bar on click for user to search for products
 * @param {bool} setIsActive allows user to use search bar on click
 * @returns button used to toggle visibility of search bar
 */
const SearchBarToggle = ({ setIsActive }) => (
  <div className={styles.searchIcon}>
    <button
      className={styles.searchIconButton}
      type="button"
      alt="Search"
      title="Search"
      onClick={() => setIsActive(true)}
    >
      <SearchIcon fontSize="large" />
    </button>
  </div>
);

export default SearchBarToggle;
