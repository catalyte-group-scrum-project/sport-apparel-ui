import { React, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import SearchIcon from '@material-ui/icons/Search';
import styles from './SearchResult.module.css';
import { ProductContext } from '../product-page/ProductContext';

/**
 * @name SearchByClick
 * @description used to send a search request and hide the visibility of search bar
 * @param {bool} setIsActive sets search bar visibility status
 * @param {string} searchTerm user inputed search term
 * @returns button to make the request to the backend
 */
const SearchByClick = ({ setIsActive }) => {
  const { setCurrentPage, searchedTerm } = useContext(ProductContext);
  const history = useHistory();
  return (
    <div className={styles.searchIconSend}>
      <SearchIcon
        fontSize="large"
        onClick={() => {
          if (searchedTerm !== '') {
            history.push('/products');
          }
          setIsActive(false);
          setCurrentPage(1);
        }}
      />
    </div>
  );
};
export default SearchByClick;
