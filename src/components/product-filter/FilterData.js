import React, { useEffect, useState, useContext } from 'react';
import { MdOutlineKeyboardArrowDown, MdOutlineKeyboardArrowUp, MdCircle } from 'react-icons/md';
import styles from './FilterData.module.css';
import {
  fetchProductBrands,
  fetchProductCategories,
  fetchProductDemographics,
  fetchProductPrimaryColorCode,
  fetchProductMaterials
} from '../product-page/ProductPageService';
import Constants from '../../utils/constants';
import { ProductContext } from '../product-page/ProductContext';
/**
 *
 * @name SidebarData
 * @description display and contains logic to filter active status products
 * @param {*} filterSelections filter selections to pass to product page
 * @returns component
 */
/* eslint-disable no-plusplus */
const SidebarData = ({ filterSelections }) => {
  // gets all of the product attributes on the backend
  const { setSearchedTerm, setCurrentPage } = useContext(ProductContext);
  const [brands, setbrands] = useState([]);
  const [categories, setcategories] = useState([]);
  const [demographics, setDemographics] = useState([]);
  const [materials, setMaterials] = useState([]);
  const [primaryColorCodes, setPrimaryColorCodes] = useState([]);
  const [apiError, setApiError] = useState(false);
  // subMenu open/close states
  const [brandMenuOpen, setBrandMenuOpen] = useState(false);
  const brandMenuToggle = () => setBrandMenuOpen(!brandMenuOpen);

  const [categoryMenuOpen, setCategoryMenuOpen] = useState(false);
  const categoryMenuToggle = () => setCategoryMenuOpen(!categoryMenuOpen);

  const [demographicsMenuOpen, setDemographicsMenuOpen] = useState(false);
  const demographicsMenuToggle = () => setDemographicsMenuOpen(!demographicsMenuOpen);

  const [primaryColorMenuOpen, setPrimaryColorMenuOpen] = useState(false);
  const primaryColorMenuToggle = () => setPrimaryColorMenuOpen(!primaryColorMenuOpen);

  const [materialMenuOpen, setMaterialMenuOpen] = useState(false);
  const materialMenuToggle = () => setMaterialMenuOpen(!materialMenuOpen);

  const [priceMenuOpen, setPriceMenuOpen] = useState(false);
  const priceMenuToggle = () => setPriceMenuOpen(!priceMenuOpen);
  // stores the filter attribute selections
  const [attributeNames, setAttributeNames] = useState([]);
  // gets the value from the input boxes and adds them to state array, if unchecked takes them off
  const handleCheck = (e) => {
    const inTheState = [...attributeNames];
    const justChecked = e.target.value;
    const foundInTheState = inTheState.indexOf(justChecked);
    if (foundInTheState === -1) {
      inTheState.push(justChecked);
    } else {
      inTheState.splice(foundInTheState, 1);
    }
    setAttributeNames(inTheState);
  };

  const [filterErrors, setFilterErrors] = useState({
    price: ''
  });

  const validatePrice = () => {
    const mininput = document.getElementById('minPrice').value;
    const maxInput = document.getElementById('maxPrice').value;
    let priceError = '';
    if (mininput > maxInput) {
      priceError = 'Whoops, minimum price needs to be larger than maximum.';
    }
    if (priceError) {
      setFilterErrors({
        price: priceError
      });
      return false;
    }
    return true;
  };

  const sendFilterData = () => {
    setCurrentPage(1);
    // send the data sting to the http helper and replace # as the controller cannot read it
    if (validatePrice() === true) {
      setFilterErrors({
        price: ''
      });
      // get the price inputs
      const mininput = document.getElementById('minPrice').value;
      const maxInput = document.getElementById('maxPrice').value;
      // if there is a value in the input box push it to data array
      if (mininput.length > 0) {
        attributeNames.push(`MinPrice=${mininput}`);
      }
      if (maxInput.length > 0) {
        attributeNames.push(`MaxPrice=${maxInput}`);
      }
      // join the data together as a string in prep for sending to api
      const filterDataToString = attributeNames.join('&');
      // if the data array contain a price input remove it to prevent duplicates when entered again
      // min and max price always appear at the end of the array, that is why pop is used
      if (attributeNames.includes(`MaxPrice=${maxInput}`)) {
        attributeNames.pop();
      }
      if (attributeNames.includes(`MinPrice=${mininput}`)) {
        attributeNames.pop();
      }
      return filterDataToString.replace('#', '%23');
    }
    const filterDataToString = attributeNames.join('&');
    return filterDataToString.replace('#', '%23');
  };

  const clearFilterData = () => {
    const getChecked = document.querySelectorAll("input[type='checkbox']");
    for (let i = 0; i < getChecked.length; i++) {
      getChecked[i].checked = false;
    }
    attributeNames.length = 0;
    const mininput = document.getElementById('minPrice');
    const maxinput = document.getElementById('maxPrice');
    mininput.value = '';
    maxinput.value = '';
    setCurrentPage(1);
    setSearchedTerm('');
    setFilterErrors({
      price: ''
    });
  };
  // gets and sets the product attributes from the back end
  useEffect(() => {
    fetchProductBrands(setbrands, setApiError);
    fetchProductCategories(setcategories, setApiError);
    fetchProductDemographics(setDemographics, setApiError);
    fetchProductPrimaryColorCode(setPrimaryColorCodes, setApiError);
    fetchProductMaterials(setMaterials, setApiError);
  }, []);

  return (
    <div>
      {apiError && <p className={styles.errMsg} data-testid="errMsg">{Constants.API_ERROR}</p>}
      <div className={styles.listItems}>
        <h3 className={styles.dropDownHeader}>
          Brand
          <button type="button" className={styles.subMenuButton} onClick={brandMenuToggle}>
            {brandMenuOpen ? <MdOutlineKeyboardArrowUp size="1.5em" color="cornflowerblue" /> : <MdOutlineKeyboardArrowDown size="1.5em" color="cornflowerblue" />}
          </button>
        </h3>
        <div className={brandMenuOpen ? styles.subMenuClosed : styles.subMenuOpen}>
          <div className={styles.content}>
            {brands.map((brand) => (
              <li key={brand} className={styles.listItem}>
                <div className={styles.subMenuData}>
                  <input
                    type="checkbox"
                    id="brand"
                    value={`Brand=${brand}`}
                    name="brand"
                    onChange={handleCheck}
                  />
                  <label htmlFor={`custom-checkbox-${brand}`}>{brand}</label>
                </div>
              </li>
            ))}
          </div>
        </div>
        <h3 className={styles.dropDownHeader}>
          Category
          <button type="button" className={styles.subMenuButton} onClick={categoryMenuToggle}>
            {categoryMenuOpen ? <MdOutlineKeyboardArrowUp size="1.5em" color="cornflowerblue" /> : <MdOutlineKeyboardArrowDown size="1.5em" color="cornflowerblue" />}
          </button>
        </h3>
        <div className={categoryMenuOpen ? styles.subMenuClosed : styles.subMenuOpen}>
          <div className={styles.content}>
            {categories.map((category) => (
              <li key={category} className={styles.listItem}>
                <div className={styles.subMenuData}>
                  <input
                    type="checkbox"
                    id={`custom-checkbox-${category}`}
                    value={`Category=${category}`}
                    name="category"
                    onChange={handleCheck}
                  />
                  <label htmlFor={`custom-checkbox-${category}`}>{category}</label>
                </div>
              </li>
            ))}
          </div>
        </div>
        <h3 className={styles.dropDownHeader}>
          Demographic
          <button type="button" className={styles.subMenuButton} onClick={demographicsMenuToggle}>
            {demographicsMenuOpen ? <MdOutlineKeyboardArrowUp size="1.5em" color="cornflowerblue" /> : <MdOutlineKeyboardArrowDown size="1.5em" color="cornflowerblue" />}
          </button>
        </h3>
        <div className={demographicsMenuOpen ? styles.subMenuClosed : styles.subMenuOpen}>
          <div className={styles.content}>
            {demographics.map((demographic) => (
              <li key={demographic} className={styles.listItem}>
                <div className={styles.subMenuData}>
                  <input
                    type="checkbox"
                    id={`custom-checkbox-${demographic}`}
                    value={`Demographic=${demographic}`}
                    name="demographic"
                    onChange={handleCheck}
                  />
                  <label htmlFor={`custom-checkbox-${demographic}`}>{demographic}</label>
                </div>
              </li>
            ))}
          </div>
        </div>
        <h3 className={styles.dropDownHeader}>
          Color
          <button type="button" className={styles.subMenuButton} onClick={primaryColorMenuToggle}>
            {primaryColorMenuOpen ? <MdOutlineKeyboardArrowUp size="1.5em" color="cornflowerblue" /> : <MdOutlineKeyboardArrowDown size="1.5em" color="cornflowerblue" />}
          </button>
        </h3>
        <div className={primaryColorMenuOpen ? styles.subMenuClosed : styles.subMenuOpen}>
          <div className={styles.content}>
            {primaryColorCodes.map((primaryColorCode) => (
              <li key={primaryColorCode} className={styles.listItem}>
                <div className={styles.subMenuData}>
                  <input
                    type="checkbox"
                    id={`custom-checkbox-${primaryColorCode}`}
                    value={`Color=${primaryColorCode}`}
                    name="primaryColorCode"
                    onChange={handleCheck}
                  />
                  <MdCircle size="1.5em" color={primaryColorCode} className={styles.colorSwatch} />
                </div>
              </li>
            ))}
          </div>
        </div>
        <h3 className={styles.dropDownHeader}>
          Material
          <button type="button" className={styles.subMenuButton} onClick={materialMenuToggle}>
            {materialMenuOpen ? <MdOutlineKeyboardArrowUp size="1.5em" color="cornflowerblue" /> : <MdOutlineKeyboardArrowDown size="1.5em" color="cornflowerblue" />}
          </button>
        </h3>
        <div className={materialMenuOpen ? styles.subMenuClosed : styles.subMenuOpen}>
          <div className={styles.content}>
            {materials.map((material) => (
              <li key={material} className={styles.listItem}>
                <div className={styles.subMenuData}>
                  <input
                    type="checkbox"
                    id={`custom-checkbox-${material}`}
                    value={`Material=${material}`}
                    name="material"
                    onChange={handleCheck}
                  />
                  <label htmlFor={`custom-checkbox-${material}`}>{material}</label>
                </div>
              </li>
            ))}
          </div>
        </div>
        <h3 className={styles.dropDownHeader}>
          Price
          <button type="button" className={styles.subMenuButton} onClick={priceMenuToggle}>
            {priceMenuOpen ? <MdOutlineKeyboardArrowUp size="1.5em" color="cornflowerblue" /> : <MdOutlineKeyboardArrowDown MdCode size="1.5em" color="cornflowerblue" />}
          </button>
        </h3>
        <div className={priceMenuOpen ? styles.subMenuClosed : styles.subMenuOpen}>
          <div className={styles.content}>
            <div className={styles.priceInputs}>
              <input
                className={styles.input}
                type="number"
                id="minPrice"
                min="0"
              />
              -
              <input
                className={styles.input}
                type="number"
                id="maxPrice"
                min="0"
              />
            </div>
            <div className={styles.priceLabels}>
              <span>Min</span>
              <span>Max</span>
            </div>
            <div className={styles.filterErrors}>
              {filterErrors.price === 'Whoops, minimum price needs to be larger than maximum.' ? filterErrors.price : ''}
            </div>
          </div>
        </div>
        <div className={styles.buttonContainer}>
          <button className={styles.buttons} type="button" onClick={() => filterSelections(sendFilterData)}>Filter</button>
          <button className={styles.buttons} type="button" onClick={() => filterSelections(clearFilterData)}>Clear</button>
        </div>
      </div>
    </div>
  );
};

export default SidebarData;
