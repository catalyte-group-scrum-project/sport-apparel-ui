/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { Tabs, Tab, AppBar } from '@material-ui/core';
import Profile from '../profile/Profile';
import PurchaseHistory from './purchase-history/PurchaseHistory';
import WishList from './wishlist/WishList';
import UserInfo from '../profile/User';

const AccountPage = ({ history, page }) => {
  // obj that allows you to direct to a page based on which tab you click
  const tabNameToIndex = {
    0: 'profile',
    1: 'purchases',
    2: 'wishlist',
    3: 'reviews'
  };
  // obj that allows you to direct to a page based on your url
  const indexToTabName = {
    profile: 0,
    purchases: 1,
    wishlist: 2,
    reviews: 3
  };
  // sets the value state which will decide which page to render
  const [value, setValue] = React.useState(indexToTabName[page]);

  const handleChange = (event, newValue) => {
    history.push(`/account/${tabNameToIndex[newValue]}`);
    setValue(newValue);
  };

  const [user, setUser] = useState({});
  const [toggleEffect, setToggleEffect] = useState(false);
  useEffect(() => {
    UserInfo(setUser);
  }, [toggleEffect]);

  return (
    <>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange}>
          <Tab label="Profile" />
          <Tab label="Purchases" />
          <Tab label="Wishlist" />
          <Tab label="Reviews" />
        </Tabs>
      </AppBar>
      {value === 0 && (
      <Profile
        user={user}
        setUser={setUser}
        toggleEffect={toggleEffect}
        setToggleEffect={setToggleEffect}
      />
      )}
      {value === 1 && <PurchaseHistory user={user} />}
      {value === 2 && <WishList />}
      {value === 3 && <Profile /> }
    </>
  );
};
export default AccountPage;
