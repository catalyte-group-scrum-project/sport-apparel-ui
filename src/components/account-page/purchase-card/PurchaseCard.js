import * as React from 'react';
import { styled } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { toPrice } from '../../checkout-page/ReviewOrderWidgetService';
import { getProductNameById } from '../purchase-history/PurchaseHistoryService';
import styles from './PurchaseCard.module.css';

// define the expand button used on each purchase card
const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest
  })
}));

/**
 * takes in a purchase object and renders a purchase card with useful info
 * @param {object} purchase an object with various data relating to a single purchase
 * @returns renders a card with a few specific pieces of info relating to a single purchase
 */
export default function PurchaseCard({ purchase }) {
  // initialize state variables
  const [expanded, setExpanded] = React.useState(false);
  const [productNames, setProductNames] = React.useState([]);
  const [productIds, setProductIds] = React.useState([]);
  const [purchaseQty, setpurchaseQty] = React.useState([]);
  const [done, setDone] = React.useState(false);

  React.useEffect(() => {
    // use state to store an array of all product IDs from a purchase
    const tempProductIds = [];
    async function getProductIds() {
      await purchase.lineItems.map((lineItem) => (
        tempProductIds.push(lineItem.productId)));
    }
    getProductIds();
    setProductIds(tempProductIds);

    // use state to store an array of the quantity of each item in a purchase
    const tempPurchaseQty = [];
    async function getPurchaseQty() {
      await purchase.lineItems.map((lineItem) => (
        tempPurchaseQty.push(lineItem.quantity)));
    }
    getPurchaseQty();
    setpurchaseQty(tempPurchaseQty);

    // pass each product ID to API call and use state to
    // store an array of the corresponding product names
    async function fetchProductNames() {
      // make non-state copy of state variables
      const fetchedProductIds = [...productIds];
      const fetchedProductNames = [...productNames];
      fetchedProductIds.map((productId) => (
        getProductNameById(productId, setProductNames, fetchedProductNames, setDone, done)
      ));
    }
    fetchProductNames();
    setDone(true);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [done]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const date = new Date(purchase.orderDate);
  const purchaseDate = date.toDateString();

  return (
    <Card sx={{ maxWidth: 345 }} className={styles.cardContainer}>
      <CardContent className={styles.container}>
        <div className={styles.verticalFlex}>
          <Typography paragraph>
            <b>Date of Purchase:</b>
            {' '}
            {purchaseDate}
          </Typography>
          <Typography paragraph>
            <b>Cost of Purchase:</b>
            {' '}
            {toPrice(purchase.purchasePrice)}
          </Typography>
        </div>
        <div className={styles.verticalFlex}>
          <Typography paragraph>
            <ol><b>Item(s) Purchased:</b></ol>
            {' '}
            {!expanded ? `(${purchaseQty[0]}) ${productNames[0]}` : productNames.map((productName, i) => (
              <Typography paragraph>{`(${purchaseQty[i]}) ${productName}`}</Typography>
            ))}
          </Typography>
        </div>
      </CardContent>
      <CardActions disableSpacing>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>
            <b>Promo Used:</b>
            {' '}
            None
          </Typography>
          <Typography paragraph>
            <b>Promo Savings:</b>
            {' '}
            N/A
          </Typography>
          <div className={styles.container}>
            <Typography paragraph className={styles.addressContainer}>
              <b>Delivery Address:</b>
              {purchase.deliveryAddress.deliveryStreet}
              {purchase.deliveryAddress.deliveryStreet2 && <br />}
              {purchase.deliveryAddress.deliveryStreet2}
              <br />
              {`${purchase.deliveryAddress.deliveryCity}, ${purchase.deliveryAddress.deliveryState} ${purchase.deliveryAddress.deliveryZip}`}
            </Typography>
            <Typography paragraph className={styles.addressContainer}>
              <b>Billing Address:</b>
              {purchase.billingAddress.billingStreet}
              {purchase.billingAddress.billingStreet2 && <br />}
              {purchase.billingAddress.billingStreet2}
              <br />
              {`${purchase.billingAddress.billingCity}, ${purchase.billingAddress.billingState} ${purchase.billingAddress.billingZip}`}
            </Typography>
          </div>
        </CardContent>
      </Collapse>
    </Card>
  );
}
