/* eslint-disable max-len */
import HttpHelper from '../../../utils/HttpHelper';
import Constants from '../../../utils/constants';

/**
 * uses the HTTP helper to make a GET request to the API for all purchases associated with the email of the user passed in
 * @param {function} setPurchases sets the state variable purchases
 * @param {string} userEmail email address of the user currently signed in
 * @param {function} setApiError sets the state variable for API errors
 */
export async function getPurchaseHistory(setPurchases, userEmail, setApiError) {
  const userPurchasEndpoint = `${Constants.PURCHASE_ENDPOINT}/${userEmail}`;

  await HttpHelper(userPurchasEndpoint, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setPurchases)
    .catch(() => {
      setApiError(true);
    });
}

/**
 * uses the HTTP helper to make a GET request to the API that returns the name of a product associated with the product ID passed in
 * @param {int} productId unique product identifier
 * @param {*} setProductNames sets the state variable productNames
 * @param {*} fetchedProductNames a non-state copy of productNames
 */
export async function getProductNameById(productId, setProductNames, fetchedProductNames) {
  const productIdEndpoint = `${Constants.ALL_PRODUCTS_ENDPOINT}/${productId}`;
  const response = await HttpHelper(productIdEndpoint, 'GET');
  const product = await response.json();
  fetchedProductNames.push(product.name);
  setProductNames(fetchedProductNames);
}
