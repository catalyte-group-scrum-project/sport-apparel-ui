/* eslint-disable max-len */
/* eslint-disable arrow-body-style */
import React, { useState, useEffect } from 'react';
import { getPurchaseHistory } from './PurchaseHistoryService';
import Constants from '../../../utils/constants';
import PurchaseCard from '../purchase-card/PurchaseCard';
import styles from './PurchaseHistory.module.css';

/**
 * creates a page that will render cards for all purchases associated with a given email address
 * @param {object} user a json object representing a user
 * @returns JSX that renders the purchase cards
 */
const PurchaseHistory = ({ user }) => {
  // initialize state variables
  const [purchases, setPurchases] = useState([]);
  const [apiError, setApiError] = useState(false);

  // call to API for purchases
  useEffect(() => {
    getPurchaseHistory(setPurchases, user.email, setApiError);
  }, [user.email]);

  const hasPurchases = purchases.length > 0;

  return (
    <div>
      <h2 className={styles.titleContainer}>
        {user.firstName}
        {' '}
        {user.lastName}
        &apos;s Purchase History
      </h2>
      <br />
      <div className={styles.cardArrangement}>
        <div className={styles.cardCollection}>
          {purchases.map((purchase) => (
            <PurchaseCard
              key={purchase.id}
              purchase={purchase}
              setApiError={setApiError}
            />
          ))}
        </div>
      </div>
      <div className={styles.centeredMessage}>{hasPurchases ? null : <p> You haven&apos;t made any purchases yet, but there&apos;s a first time for everything! </p>}</div>
      <div className={styles.centeredMessage}>
        {hasPurchases ? null : (
          <p>
            Find something you like on our
            {' '}
            <a href="/products">products</a>
            {' '}
            page.
          </p>
        )}

      </div>
      <div className={styles.centeredMessage}>
        {apiError && (
        <p>
          {' '}
          {Constants.API_ERROR}
          {' '}
        </p>
        )}
      </div>
    </div>
  );
};

export default PurchaseHistory;
