import React, { useEffect, useState } from 'react';
import { fetchPaginatedWishListMaxNumber, getPaginatedWishList } from './WishListService';
import styles from './WishList.module.css';
import ProductCard from '../../product-card/ProductCard';
import CallToastContainer from '../../toast/toast';
/**
 * Displays all of your liked products as cards on the wishlist
 * @param {array} likedProductIds an array of ids if products that have been liked by the user
 * @returns component to view wishlist
 */
const WishList = () => {
  const [maxNumber, setMaxNumber] = useState(Number);
  const [currentPage, setCurrentPage] = useState(1);
  // state that will hold all of the liked products, used to make wishlist cards
  const [likedProducts, setLikedProducts] = useState([]);
  useEffect(() => {
    getPaginatedWishList(setLikedProducts, currentPage);
  }, [currentPage]);

  const maxPage = (Math.ceil(maxNumber / 20));
  useEffect(() => {
    fetchPaginatedWishListMaxNumber(setMaxNumber);
  }, []);
  let startPage;
  let endPage;
  if (maxPage <= 10) {
    // less than 10 total pages so show all
    startPage = 1;
    endPage = maxPage;
  } else if (currentPage <= 6) {
    // more than 10 total pages so calculate start and end pages
    startPage = 1; endPage = 10;
  } else if (currentPage + 4 >= maxPage) {
    startPage = maxPage - 9;
    endPage = maxPage;
  } else {
    startPage = currentPage - 5;
    endPage = currentPage + 4;
  }

  const pages = [...Array((endPage + 1) - startPage).keys()].map((i) => (startPage + i));

  return (
    <div>
      <CallToastContainer />
      <div className={styles.app}>
        {likedProducts.map((product) => (
          <div key={product.id}>
            <ProductCard
              product={product}
            />
          </div>
        ))}
      </div>
      <div className={styles.spacing} />
      <footer className={styles.navigation}>
        <div className={styles.paginationContainer} hidden={maxPage === 1 || maxPage === 0}>
          <ul className={styles.pagination}>
            <li className={styles.paginationLi}>
              <div>
                <button className={styles.first} type="button" onClick={() => setCurrentPage(1)} disabled={currentPage === 1}>
                  First
                </button>
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                <button className={styles.prev} type="button" onClick={() => setCurrentPage(currentPage - 1)} disabled={currentPage === 1}>
                  Previous
                </button>
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                {pages.map((pageNumber) => (
                  <button className={styles.pageSelector} type="button" onClick={() => setCurrentPage(pageNumber)} disabled={currentPage === pageNumber}>
                    {pageNumber}
                  </button>
                ))}
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                <button className={styles.next} type="button" onClick={() => setCurrentPage(currentPage + 1)} disabled={currentPage === maxPage}>
                  Next
                </button>
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                <button className={styles.last} type="button" onClick={() => setCurrentPage(maxPage)} disabled={currentPage === maxPage}>
                  Last
                </button>
              </div>
            </li>
          </ul>
        </div>
      </footer>
    </div>
  );
};

export default WishList;
