import { toast } from 'react-toastify';
import constants from '../../../utils/constants';
import HttpHelper from '../../../utils/HttpHelper';
import CallToastContainer from '../../toast/toast';
import { updateLatestView, updateProductViews } from '../../product-page/ProductPageService';

/**
 * adds a product to the wishlist in the database using a HTTP post request
 * @param {obj} product that we will be adding to the wishlist
 * @param {func} displayLikedButton a function that will display the like button on the header
 */
export async function addToWishList(product, displayLikedButton) {
  const email = await fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${sessionStorage.getItem('token')}`)
    .then((res) => res.json())
    .then((user) => (user.email));
  if (email) {
    const WishListItem = {
      productId: product.id,
      userEmail: email,
      name: product.name,
      description: product.description,
      imageSrc: product.imageSrc,
      price: product.price,
      demographic: product.demographic,
      category: product.category,
      type: product.type
    };
    let currentTime = 0;
    const handelTime = () => {
      currentTime = new Date().toUTCString();
      return currentTime;
    };
    await HttpHelper('/WishList', 'POST', WishListItem)
      .then((response) => {
        response.json();
        if (response.ok) {
          CallToastContainer('Product added to wishlist!', 3000, true, toast.POSITION.TOP_CENTER, toast.success);
          displayLikedButton();
          handelTime();
          if (product.latestView !== currentTime && (product.latestView + 60000) <= currentTime) {
            updateProductViews(product.id);
            updateLatestView(product.id);
          }
        }
      })
      .catch(() => {
        CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
        return false;
      });
  } else {
    CallToastContainer('YOURE NOT SIGNED IN!!!', 5000, true, toast.POSITION.TOP_CENTER, toast.error);
  }
}
/**
 * @name setLikedProducts
 * @description Utilizes Http Helper to get all of the liked products for a specific user
 * @param {func} setLikedProducts a function that sets the likedProducts state
 * @returns sets state for likedProducts attributes if 200 response, else, throw an error
 */
export async function getLikedProducts(setLikedProducts) {
  const email = await fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${sessionStorage.getItem('token')}`)
    .then((res) => res.json())
    .then((user) => (user.email));
  if (email) {
    await HttpHelper(`/WishList/${email}`, 'GET')
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('getReviews is working incorrectly');
      })
      .then(setLikedProducts)
      .catch(() => {
        CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      });
  }
}
/**
 * @name setLikedProductIds
 * @description HTTP get request that will retrieve liked product ids from the db
 * @param {func} setLikedProductIds a function that will set the likedProductsIds state
 * @returns sets state for LikedProductIds state if 200, else throws API error and
 */
export async function getLikedProductIds(setLikedProductIds) {
  const email = await fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${sessionStorage.getItem('token')}`)
    .then((res) => res.json())
    .then((user) => (user.email));
  if (email) {
    await HttpHelper(`/WishList/${email}`, 'GET')
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(constants.API_ERROR);
      })
      .then((body) => {
        const idsOfLikedProducts = [];
        body.map((likedProduct) => idsOfLikedProducts.push(likedProduct.productId));
        setLikedProductIds(idsOfLikedProducts);
      })
      .catch(() => {
        CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      });
  } else {
    setLikedProductIds([]);
  }
}
/**
 *
 * @name getPaginatedWishList
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setLikedProducts sets state for products
 * @param {*} pageNumber current page number for pagination
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function getPaginatedWishList(setLikedProducts, pageNumber) {
  const email = await fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${sessionStorage.getItem('token')}`)
    .then((res) => res.json())
    .then((user) => (user.email));
  if (email) {
    await HttpHelper(`/WishList/filter/${email}?pageNumber=${pageNumber}`, 'GET')
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(constants.API_ERROR);
      })
      .then(setLikedProducts)
      .catch(() => {
        CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      });
  }
}
/**
 *
 * @name fetchPaginatedWishListMaxNumber
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setMaxNumber sets max product number
 * @returns sets state for max number if 200 response, else sets state for apiError
 */
export async function fetchPaginatedWishListMaxNumber(setMaxNumber) {
  const email = await fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${sessionStorage.getItem('token')}`)
    .then((res) => res.json())
    .then((user) => (user.email));
  if (email) {
    await HttpHelper(`/WishList/maxNumber/${email}`, 'GET')
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(constants.API_ERROR);
      })
      .then(setMaxNumber)
      .catch(() => {
        CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      });
  }
}
