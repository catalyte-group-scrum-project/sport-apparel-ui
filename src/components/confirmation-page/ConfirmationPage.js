import React from 'react';
import { useCart } from '../checkout-page/CartContext';

/**
 * @name ConfirmationPage
 * @description Displays Order Confirmation
 * @return component
 */
const ConfirmationPage = () => {
  const {
    state: { products }
  } = useCart();
  if (products.length !== 0) {
    window.location.reload(false);
  }
  return (
    <div>
      Order success!
    </div>
  );
};

export default ConfirmationPage;
