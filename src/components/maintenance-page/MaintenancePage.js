/* eslint-disable max-len */
import React, { useEffect, useState } from 'react';
<<<<<<< HEAD
<<<<<<< HEAD
=======
import { useHistory } from 'react-router-dom';
>>>>>>> 3cd3349f19af760ce835af4091b8a05c448b6af1
import { toast } from 'react-toastify';
=======
import StickyTable from 'react-sticky-table-thead';
import { useHistory } from 'react-router-dom';
>>>>>>> 5cee39ccf227d7e9ef6f94ab9b26b79dcf5b9dc2
import styles from './MaintenancePage.module.css';
import { fetchAndFilterProducts, getReviews } from './MaintenancePageService';
import 'react-toastify/dist/ReactToastify.css';
import CallToastContainer from '../toast/toast';
import PromoModal from './prom-modal/PromoModal';
import ReadOnlyRow from './edit-product/ReadOnlyRow';
import EditableRow from './edit-product/EditableRow';
import editProduct from './edit-product/EditProductService';
import { getLineItems } from './delete-product/MaintenancePageDeleteService';
/* eslint-disable no-plusplus */
/**
 * @name MaintenancePage
 * @description products list in table view for optimized viewing and editing
 * @return component
 */
const MaintenancePage = () => {
  // defines and tracks the array of product data for the table
  const [products, setProducts] = useState([]);
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
=======
  const history = useHistory();
>>>>>>> 3cd3349f19af760ce835af4091b8a05c448b6af1
=======
>>>>>>> 5cee39ccf227d7e9ef6f94ab9b26b79dcf5b9dc2

=======
>>>>>>> op-33-view-product-reviews
  // defines and tracks the order data appears in for sorting function, in this case ascending order.
  const [order, setOrder] = useState(true);
  const [deleteSwitch, setDeleteSwitch] = useState(true);

  const history = useHistory();

  const [lineItems, setLineItems] = useState([]);
  const [reviewedProducts, setReviewedProducts] = useState([]);
  // fetches all products from the API to populate the table by passing in tracked data (setProducts) defined in useState.
  useEffect(() => {
    fetchAndFilterProducts(setProducts);
    getReviews(setReviewedProducts);
    getLineItems(setLineItems);
  }, [deleteSwitch]);

  // stickytable enables fixed table header
  // onclick enables the defined sorting function for specified column heads
  const sorting = (col) => {
    if (order === true) {
      const sorted = ([...products].sort((a, b) => (a[col] > b[col] ? 1 : -1)));
      setProducts(sorted);
      setOrder(false);
    }
    if (order === false) {
      const sorted = ([...products].sort((a, b) => (a[col] < b[col] ? 1 : -1)));
      setProducts(sorted);
      setOrder(true);
    }
  };
  // edit form data state and corresponding  edited product id
  const [editProductId, setEditProductId] = useState(null);
  const [editFormData, setEditFormData] = useState({
    name: '',
    brand: '',
    quantity: '',
    description: '',
    demographic: '',
    category: '',
    type: '',
    primaryColorCode: '',
    secondaryColorCode: '',
    active: ''
  });
  // triggers click for edit product button for corresponding product
  const handleEditClick = (event, product) => {
    event.preventDefault();
    setEditProductId(product.id);

    const formValues = {
      name: product.name,
      dateCreated: product.dateCreated,
      dateModified: product.dateModified,
      brand: product.brand,
      sku: product.sku,
      quantity: product.quantity,
      description: product.description,
      demographic: product.demographic,
      material: product.material,
      imageSrc: product.imageSrc,
      price: product.price,
      category: product.category,
      type: product.type,
      releaseDate: product.releaseDate,
      primaryColorCode: product.primaryColorCode,
      secondaryColorCode: product.secondaryColorCode,
      styleNumber: product.styleNumber,
      globalProductCode: product.globalProductCode,
      active: product.active
    };

    setEditFormData(formValues);
  };
  // handles the form change
  // might not be needed anymore
  const handleEditFormChange = (event) => {
    event.preventDefault();
    const fieldName = event.target.getAttribute('name');
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;

    setEditFormData(newFormData);
  };
  // intial product errors for when editing product
  const [productErrors, setProductErrors] = React.useState({
    primaryColor: '', secondaryColor: ''
  });
  // array to temporarily store input values to update products with
  const productEdited = [];
  // validate edit meets requirements
  const validateChanges = () => {
    let primaryColorError = '';
    let secondaryColorError = '';
    const colorCodeReg = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/; //eslint-disable-line
    if (!colorCodeReg.test(productEdited[8])) {
      primaryColorError = 'Please enter a vaild color code.';
    }
    if (!colorCodeReg.test(productEdited[9])) {
      secondaryColorError = 'Please enter a vaild color code.';
    }
    if (primaryColorError || secondaryColorError) {
      setProductErrors({
        primaryColor: primaryColorError,
        secondaryColor: secondaryColorError
      });
      return false; // if errors show, return false
    }
    return true; // if no errors show, return true
  };
  // submit edited product to database
  const handleEditFormSubmit = (event) => {
    event.preventDefault();
    // gets current date to update modifed date upon succesful edit
    const updateModifiedDate = new Date().toISOString();
    // grab all of the text input values
    const inputName = document.querySelectorAll("input[type='text']");
    for (let i = 0; i < inputName.length; i++) {
      productEdited.push(inputName[i].value);
    }
    // grab all of the number input values
    const numberInput = document.querySelectorAll("input[type='number']");
    for (let x = 0; x < numberInput.length; x++) {
      productEdited.push(numberInput[x].value);
    }
    // grab all of the date value
    const dateInput = document.querySelectorAll("input[type='date']");
    for (let d = 0; d < dateInput.length; d++) {
      productEdited.push(dateInput[d].value);
    }
    // if the field is empty replace with original value
    if (productEdited[0] === '') {
      productEdited.splice(0, 1, editFormData.name);
    }
    if (productEdited[1] === '') {
      productEdited.splice(1, 1, editFormData.brand);
    }
    if (productEdited[2] === '') {
      productEdited.splice(2, 1, editFormData.description);
    }
    if (productEdited[3] === '') {
      productEdited.splice(3, 1, editFormData.material);
    }
    if (productEdited[4] === '') {
      productEdited.splice(4, 1, editFormData.imageSrc);
    }
    if (productEdited[5] === '') {
      productEdited.splice(5, 1, editFormData.demographic);
    }
    if (productEdited[6] === '') {
      productEdited.splice(6, 1, editFormData.category);
    }
    if (productEdited[7] === '') {
      productEdited.splice(7, 1, editFormData.type);
    }
    if (productEdited[8] === '') {
      productEdited.splice(8, 1, editFormData.primaryColorCode);
    }
    if (productEdited[9] === '') {
      productEdited.splice(9, 1, editFormData.secondaryColorCode);
    }
    if (productEdited[10] === '') {
      productEdited.splice(10, 1, editFormData.quantity);
    }
    if (productEdited[11] === '') {
      productEdited.splice(11, 1, editFormData.price);
    }
    if (productEdited[12] === '') {
      productEdited.splice(12, 1, editFormData.releaseDate);
    }
    // converts string to bool to send to back end
    if (editFormData.active === 'true') {
      editFormData.active = true;
    }
    // converts string to bool to send to back end
    if (editFormData.active === 'false') {
      editFormData.active = false;
    }
    // assign edited product a value to send to backend
    const editedProduct = {
      id: editProductId,
      name: productEdited[0],
      dateCreated: editFormData.dateCreated,
      dateModified: updateModifiedDate,
      brand: productEdited[1],
      sku: editFormData.sku,
      quantity: productEdited[10],
      description: productEdited[2],
      material: productEdited[3],
      imageSrc: productEdited[4],
      price: productEdited[11],
      demographic: productEdited[5],
      category: productEdited[6],
      type: productEdited[7],
      releaseDate: productEdited[12],
      primaryColorCode: productEdited[8],
      secondaryColorCode: productEdited[9],
      styleNumber: editFormData.styleNumber,
      globalProductCode: editFormData.globalProductCode,
      active: editFormData.active
    };
    const newProducts = [...products];
    const index = products.findIndex((product) => product.id === editProductId);

    newProducts[index] = editedProduct;
    if (validateChanges() === true) {
      // change product without refresh
      setProducts(newProducts);
      // change back to read only view
      setEditProductId(null);
      // send PUT request
      editProduct(editedProduct);
      setProductErrors({ // resets error messages
        primaryColor: '', secondaryColor: ''
      });
    } else {
      event.preventDefault();
    }
  };
  // sets back to read only row
  const handleCancelClick = () => {
    setEditProductId(null);
  };
  const [openModal, setOpenModal] = useState(false);
  return (
    <div>
<<<<<<< HEAD
<<<<<<< HEAD
      <button className={styles.success} onClick={generateToast} type="button">Sucess</button>
      <button className={styles.warning} onClick={generateToast} type="button">Information</button>
      <button className={styles.failure} onClick={generateToast} type="button">Failure</button>
<<<<<<< HEAD
=======
=======
>>>>>>> 5cee39ccf227d7e9ef6f94ab9b26b79dcf5b9dc2
      <button className={styles.createButton} onClick={() => { history.push('/maintenance/create-product'); }} type="submit">Create</button>
>>>>>>> 3cd3349f19af760ce835af4091b8a05c448b6af1
=======
      <div className={styles.buttonContainer}>
        <button className={styles.createButton} onClick={() => { history.push('/maintenance/create-product'); }} type="submit">Create Product</button>
        <button type="button" className={styles.promoButton} onClick={() => setOpenModal(true)}>Create Promo</button>
      </div>
>>>>>>> op-33-view-product-reviews
      <CallToastContainer />
      {openModal && <PromoModal closeModal={setOpenModal} />}
      <StickyTable height={800}>
        <form onSubmit={handleEditFormSubmit}>
          <table style={{ width: '100%', borderCollapse: 'collapse' }}>
            <div className={styles.table}>
              <thead>
                <th>Action</th>
                <th onClick={() => sorting('id')}>ID</th>
                <th onClick={() => sorting('dateCreated')}>DateCreated</th>
                <th onClick={() => sorting('dateModified')}>DateModified</th>
                <th onClick={() => sorting('name')}>Name</th>
                <th onClick={() => sorting('brand')}>Brand</th>
                <th>SKU</th>
                <th onClick={() => sorting('quantity')}>Quantity</th>
                <th onClick={() => sorting('description')}>Description</th>
                <th onClick={() => sorting('material')}>Material</th>
                <th onClick={() => sorting('imageSrc')}>ImageSRC</th>
                <th onClick={() => sorting('price')}>Price</th>
                <th onClick={() => sorting('demographic')}>Demographic</th>
                <th onClick={() => sorting('category')}>Category</th>
                <th onClick={() => sorting('type')}>Type</th>
                <th onClick={() => sorting('releaseDate')}>ReleaseDate</th>
                <th>PrimaryColorCode</th>
                <th>SecondaryColorCode</th>
                <th>StyleNumber</th>
                <th>GlobalProductCode</th>
                <th onClick={() => sorting('active')}>Active</th>
              </thead>
              <tbody>
                {products.map((product) => (
                  <>
                    {editProductId === product.id ? (
                      // in line editing for products
                      <EditableRow
                        product={product}
                        editFormData={editFormData}
                        handleEditFormChange={handleEditFormChange}
                        handleCancelClick={handleCancelClick}
                        productErrors={productErrors}
                      />
                    ) : (
                      // read only for products
                      <ReadOnlyRow
                        product={product}
                        handleEditClick={handleEditClick}
                        lineItems={lineItems}
                        reviewedProducts={reviewedProducts}
                        deleteSwitch={deleteSwitch}
                        setDeleteSwitch={setDeleteSwitch}
                      />
                    )}
                  </>
                ))}
              </tbody>
            </div>
          </table>
        </form>
      </StickyTable>
    </div>
  );
};

export default MaintenancePage;
