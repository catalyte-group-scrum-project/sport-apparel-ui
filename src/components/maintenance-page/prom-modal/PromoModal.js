import React from 'react';
import { MdClose } from 'react-icons/md';
import createPromo from './PromoModalService';
import styles from './PromoModal.module.css';
import PromoModalForm from './PromoModalForm';
import CallToastContainer from '../../toast/toast';
import LoadingSpinner from '../../loading-spinner/LoadingSpinner';
/* eslint-disable max-len */
/**
 * @name PromoModal
 * @description Displays a modal form to create a promo code and POSTS the data once it is validated
 * @returns component
 */
const PromoModal = ({ closeModal }) => {
  const [data, setData] = React.useState({});
  const [errors, setErrors] = React.useState({
    title: '', description: '', rate: '', isPercent: ''
  });

  const [isLoading, setIsLoading] = React.useState(false);

  const [check, setCheck] = React.useState(false);

  const handleCheck = () => {
    setCheck(!check);
  };
  const onPromoChange = (e) => {
    setData({ ...data, [e.target.id]: e.target.value });
  };

  const validatePromo = () => {
    let titleError = '';
    let descriptionError = '';
    let rateError = '';
    const whitespaceOnlyReg = /^\s*$/;
    const decimalOrWholeNumberReg = /^[0-9]+(\.[0-9][0-9])?$/;

    if (whitespaceOnlyReg.test(data.title)
      || data.title === undefined) {
      titleError = 'Promo must have a name';
    }
    if (whitespaceOnlyReg.test(data.description)
      || data.description === undefined) {
      descriptionError = 'Description is a required field';
    }
    if (!decimalOrWholeNumberReg.test(data.rate)
      || data.rate === undefined) {
      rateError = 'Please enter a numerical value';
    }
    if (titleError || descriptionError || rateError) {
      setErrors({
        title: titleError,
        description: descriptionError,
        rate: rateError
      });
      return false;
    }
    return true;
  };

  const makePromo = (ev) => {
    if (validatePromo() === true) {
      const promo = {
        title: data.title,
        description: data.description,
        rate: data.rate,
        isPercent: check
      };
      setIsLoading(true);
      createPromo(promo); // sends the 'promo' to the database
      setErrors({
        title: '', description: '', rate: ''
      });
      setTimeout(() => { // closes modal upon submit
        closeModal(false);
        setIsLoading(false);
      }, 800);
    } else {
      ev.preventDefault(); // prevents form from submitting
    }
  };

  return (
    <div>
      {isLoading ? <LoadingSpinner /> : validatePromo}
      <div className={styles.modalBackground}>
        <div className={isLoading ? styles.modalContainerBlur : styles.modalContainer}>
          <button type="button" onClick={() => closeModal(false)} className={styles.closeButton}>
            <MdClose MdCode size="2em" color="cornflowerblue" />
          </button>
          <div className={styles.title}>
            <h1>Create New Promotional Code</h1>
          </div>
          <div className={styles.body}>
            <CallToastContainer />
            <PromoModalForm
              onChange={onPromoChange}
              data={data}
              errors={errors}
            />
            <div className={styles.percentCheck}>
              <h4>Percent</h4>
              <input
                type="checkbox"
                value={check}
                onChange={handleCheck}
              />
            </div>
          </div>
          <div className={styles.footer}>
            <button type="submit" onClick={makePromo} disabled={isLoading}>Create</button>
          </div>
        </div>
        <div className={styles.bluredBackground} />
      </div>
    </div>
  );
};
/* eslint-enable max-len */
export default PromoModal;
