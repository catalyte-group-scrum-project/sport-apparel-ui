import React from 'react';
import FormItem from '../../form/FormItem';

/**
 * @name PromoModalForm
 * @description creates a form that will gather data to create a promo code
 * @returns component
 */
const PromoModalForm = ({
  onChange, data, errors
}) => {
  const invalidInputCss = {
    border: '2px solid red'
  };
  const validInputCss = {
    border: '1px solid black'
  };

  return (
    <div>
      <FormItem
        placeholder="DISCOUNT"
        type="text"
        id="title"
        label="Promo Code Name"
        onChange={onChange}
        value={data.title}
        errorMsg={errors.title}
        inputStyle={errors.title ? invalidInputCss : validInputCss}
      />
      <FormItem
        placeholder="Take $5 off of your order!"
        type="text"
        id="description"
        label="Description"
        onChange={onChange}
        value={data.description}
        errorMsg={errors.description}
        inputStyle={errors.description ? invalidInputCss : validInputCss}
      />
      <FormItem
        placeholder="5.00"
        type="text"
        id="rate"
        label="Rate"
        onChange={onChange}
        value={data.rate}
        errorMsg={errors.rate}
        inputStyle={errors.rate ? invalidInputCss : validInputCss}
      />
    </div>
  );
};
export default PromoModalForm;
