import { toast } from 'react-toastify';
import HttpHelper from '../../../utils/HttpHelper';
import Constants from '../../../utils/constants';
import CallToastContainer from '../../toast/toast';

/**
 * This function will take in the promo code and send that promo code into the database
 * @name SetPromo
 * @description Utilizes HttpHelper to make a post request to an API
 * @param {*} promo the promo code data that needs to be pushed into the database
 * @returns a promo code into the database
 */
export default async function createPromo(promo) {
  await HttpHelper(Constants.PROMO_CODE_ENDPOINT, 'POST', promo)
    .then((response) => {
      response.json();
      if (response.ok) {
        setTimeout(() => {
          CallToastContainer('Promo Created!', 6000, true, toast.POSITION.TOP_CENTER, toast.success);
        }, 500);
      }
    })
    .catch(() => {
      CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      return false;
    });
}
