import { toast } from 'react-toastify';
import HttpHelper from '../../utils/HttpHelper';
import CallToastContainer from '../toast/toast';
import constants from '../../utils/constants';
/**
 *
 * @name fetchAndFilterProducts
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function fetchAndFilterProducts(setProducts) {
  await HttpHelper(constants.ALL_PRODUCTS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(constants.API_ERROR);
    })
    .then((body) => {
      const id = 'id';
      body.sort((a, b) => (a[id] > b[id] ? 1 : -1));
      setProducts(body);
    })
    .catch(() => {
      CallToastContainer('Unable to connect to database, Please try again later.', 3000, true, toast.POSITION.TOP_CENTER, toast.error);
    });
}
export async function getReviews(setReviewedProducts) {
  await HttpHelper('/reviews', 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('getReviews is working incorrectly');
    })
    .then((body) => {
      const idsOfProductsWithReviews = [];
      body.map((review) => idsOfProductsWithReviews.push(review.productId));
      setReviewedProducts(idsOfProductsWithReviews);
    });
}
