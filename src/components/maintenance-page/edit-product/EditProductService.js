import { toast } from 'react-toastify';
import CallToastContainer from '../../toast/toast';
import HttpHelper from '../../../utils/HttpHelper';
/**
 *
 * @name editedProduct
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} product product to be updated
 * @returns updates product if 200 response, else calls error
 */
export default async function editProduct(product) {
  await HttpHelper(`/products/${product.id}`, 'PUT', product)
    .then((response) => {
      response.json();
      if (response.ok) {
        setTimeout(() => {
          CallToastContainer('Product has been updated successfully', 4000, true, toast.POSITION.TOP_CENTER, toast.success);
        }, 500);
      }
    })
    .catch(() => {
      CallToastContainer('Unable to connect to database, Please try again later.', 6000, true, toast.POSITION.TOP_CENTER, toast.error);
      return false;
    });
}
