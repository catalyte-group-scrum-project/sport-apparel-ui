import React from 'react';
import { MdOutlineModeEditOutline } from 'react-icons/md';
import DeleteProductButton from '../delete-product/DeleteProductButton';
import styles from '../MaintenancePage.module.css';
/* eslint-disable arrow-body-style */
/**
 * @name ReadOnlyRow
 * @description rows and table data of products and actions to interact with the products
 * @returns rows and table data of products from the data base
 */
const ReadOnlyRow = ({
  product,
  handleEditClick,
  lineItems,
  reviewedProducts,
  deleteSwitch,
  setDeleteSwitch
}) => {
  return (
    <tr>
      <td>
        <button className={styles.actionButtons} type="button">
          <MdOutlineModeEditOutline
            size="1em"
            onClick={(event) => handleEditClick(event, product)}
          />
        </button>
        {!reviewedProducts.includes(product.id)
          ? (
            <DeleteProductButton
              className="deleteButton"
              lineItems={lineItems}
              productToDeleteId={product.id}
              productActiveStatus={product.active}
              setDeleteSwitch={setDeleteSwitch}
              deleteSwitch={deleteSwitch}
            />
          )
          : (
            ''
          )}
      </td>
      <td>{product.id}</td>
      <td>{product.dateCreated}</td>
      <td>{product.dateModified}</td>
      <td>{product.name}</td>
      <td>{product.brand}</td>
      <td>{product.sku}</td>
      <td>{product.quantity}</td>
      <td>{product.description}</td>
      <td>{product.material}</td>
      <td>{product.imageSrc}</td>
      <td>{product.price}</td>
      <td>{product.demographic}</td>
      <td>{product.category}</td>
      <td>{product.type}</td>
      <td>{product.releaseDate}</td>
      <td>{product.primaryColorCode}</td>
      <td>{product.secondaryColorCode}</td>
      <td>{product.styleNumber}</td>
      <td>{product.globalProductCode}</td>
      <td>{product.active.toString()}</td>
    </tr>
  );
};

export default ReadOnlyRow;
