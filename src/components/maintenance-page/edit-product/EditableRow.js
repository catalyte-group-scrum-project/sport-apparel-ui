import React from 'react';
import { MdCheck, MdClose } from 'react-icons/md';
import styles from '../MaintenancePage.module.css';

/**
 *
 * @name EditableRow
 * @description an input form that appears in table row that is used to update a product
 * @returns product information to update the product with the id in that row.
 */
const EditableRow = ({
  product,
  editFormData,
  handleEditFormChange,
  handleCancelClick,
  productErrors
}) => {
  // bool values for active status
  const activeValues = [
    true,
    false
  ];

  return (
    <tr>
      <td>
        <button type="submit" className={styles.actionButtons}>
          <MdCheck size="1em" />
        </button>
        <button type="button" onClick={handleCancelClick} className={styles.actionButtons}>
          <MdClose size="1em" />
        </button>
      </td>
      <td>{product.id}</td>
      <td>{product.dateCreated}</td>
      <td>{product.dateModified}</td>
      <td><input type="text" name="name" placeholder={editFormData.name} /></td>
      <td><input type="text" name="brand" placeholder={editFormData.brand} /></td>
      <td>{product.sku}</td>
      <td><input type="number" min="0" name="quantity" placeholder={editFormData.quantity} /></td>
      <td><input type="text" name="description" placeholder={editFormData.description} /></td>
      <td><input type="text" name="material" placeholder={editFormData.material} /></td>
      <td><input type="text" name="imageSrc" style={{ width: '50em' }} placeholder={product.imageSrc} /></td>
      <td><input type="number" name="price" step="any" min="0" placeholder={editFormData.price} /></td>
      <td><input type="text" name="demographic" placeholder={editFormData.demographic} /></td>
      <td><input type="text" name="category" placeholder={editFormData.category} /></td>
      <td><input type="text" name="type" placeholder={editFormData.type} /></td>
      <td><input type="date" name="releaseDate" placeholder={product.releaseDate} /></td>
      <td>
        <input type="text" name="primaryColorCode" placeholder={editFormData.primaryColorCode} />
        <div style={{
          color: 'red', fontSize: '12px', height: '0em', marginTop: '4px'
        }}
        >
          {productErrors.primaryColor === 'Please enter a vaild color code.' ? productErrors.primaryColor : ''}
        </div>
      </td>
      <td>
        <input type="text" name="secondaryColorCode" placeholder={editFormData.secondaryColorCode} />
        <div style={{
          color: 'red', fontSize: '12px', height: '0em', marginTop: '4px'
        }}
        >
          {productErrors.secondaryColor === 'Please enter a vaild color code.' ? productErrors.secondaryColor : ''}
        </div>
      </td>
      <td>{product.styleNumber}</td>
      <td>{product.globalProductCode}</td>
      <td>
        <select name="active" onChange={handleEditFormChange} value={editFormData.active}>
          <option value={activeValues[0]}>true</option>
          <option value={activeValues[1]}>false</option>
        </select>
      </td>
    </tr>
  );
};
export default EditableRow;
