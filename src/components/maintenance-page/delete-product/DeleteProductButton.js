import React, { useState } from 'react';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { toast } from 'react-toastify';
import { deleteProduct, makeProductInactive } from './MaintenancePageDeleteService';
import ProductHasBeenPurchasedModal from './ProductHasBeenPurchasedModal';
import ConfirmDeleteProductModal from './ConfirmDeleteProductModal';
import CallToastContainer from '../../toast/toast';
import styles from '../MaintenancePage.module.css';
/* eslint-disable max-len */
/* eslint-disable react/jsx-one-expression-per-line */
/**
 * @name DeleteProductButton
 * @description displays a button that will delete the product of the id given to it
 * @param {Int} productToDeleteId the id of the product that needs to be deleted
 * @return component
 */
const DeleteProductButton = ({
  lineItems,
  productToDeleteId,
  productActiveStatus,
  setDeleteSwitch,
  deleteSwitch
}) => {
  // state that will decide wheather to display a modal telling the user that a product has been purchased
  const [productHasBeenPurchasedModal, setProductHasBeenPurchasedModal] = useState(false);

  // state that will decide wheather to display a modal asking the user if he is sure that he would like to delete the purchase
  const [displayConfirmDeleteProductModal, setDisplayConfirmDeleteProductModal] = useState(false);

  const handleProductHasBeenPurchaseModal = () => {
    setProductHasBeenPurchasedModal(!productHasBeenPurchasedModal);
  };

  const handelDisplayConfirmDeleteProducModal = () => {
    setDisplayConfirmDeleteProductModal(!displayConfirmDeleteProductModal);
  };

  // function that will run when a delete box has been clicked, this function will check if the product has been part of any purchases
  // and then toggle a modal based on whather the product has been purchased or not.
  const removeProduct = () => {
    const idsOfProductsBeingBought = [];
    lineItems.map((lineItem) => idsOfProductsBeingBought.push(lineItem.productId)); // turn lineItems into an array of productId: 's
    if (!idsOfProductsBeingBought.includes(productToDeleteId)) {
      setDisplayConfirmDeleteProductModal(true);
    } else {
      CallToastContainer('Product has been purchased', 1500, true, toast.POSITION.TOP_CENTER, toast.error);
      if (productActiveStatus) {
        setProductHasBeenPurchasedModal(true);
      }
    }
  };

  // function that will run a PUT request to make the product inactive
  const makeProductStatusInactive = () => {
    makeProductInactive(productToDeleteId);
    setProductHasBeenPurchasedModal(false);
  };

  return (
    <div>{productHasBeenPurchasedModal
      ? (
        <ProductHasBeenPurchasedModal
          closeModal={handleProductHasBeenPurchaseModal}
          makeProductStatusInactive={() => {
            makeProductStatusInactive();
            setDeleteSwitch(!deleteSwitch);
          }}
        />
      )
      : (
        <button type="button" onClick={() => { removeProduct(); }} className={styles.actionButtons}>
          <DeleteOutlineIcon fontSize="small" />
        </button>
      )}
      <div>{displayConfirmDeleteProductModal ? (
        <ConfirmDeleteProductModal
          closeModal={handelDisplayConfirmDeleteProducModal}
          deleteProduct={() => {
            deleteProduct(productToDeleteId);
            handelDisplayConfirmDeleteProducModal();
            setDeleteSwitch(!deleteSwitch);
          }}
        />
      )
        : <div />}
        <CallToastContainer />
      </div>
    </div>
  );
};
export default DeleteProductButton;
