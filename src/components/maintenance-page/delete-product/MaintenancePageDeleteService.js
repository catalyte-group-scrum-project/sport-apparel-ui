import { toast } from 'react-toastify';
import HttpHelper from '../../../utils/HttpHelper';
import CallToastContainer from '../../toast/toast';
/**
 * a function that will delete a product from the database
 * @param {int} id id of the product that needs to be deleted
 * @returns true or false
 */
export async function deleteProduct(id) {
  await HttpHelper(`/products/delete/${id}`, 'DELETE')
    .then((response) => {
      response.json();
      if (response.ok) {
        CallToastContainer('Successfully Deleted!', 3000, true, toast.POSITION.TOP_CENTER, toast.success);
      }
    })
    .catch(() => {
      CallToastContainer('Unable to connect to database, Please try again later.', 3000, true, toast.POSITION.TOP_CENTER, toast.error);
      return false;
    });
}
/**
 * Get gets products purchased from the database
 * @param {func} setLineItems sets the lineItems state to the GET request return
 * @returns array of Line Items from the DB
 */
export async function getLineItems(setLineItems) {
  await HttpHelper('/lineItem', 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('unable to getLineItems');
    })
    .then(setLineItems);
}
/**
 * PUT request that will change that active status of a product to false
 * @param {int} id id of the product that needs to be changed
 * @returns true or false
 */
export async function makeProductInactive(id) {
  await HttpHelper(`/products/makeInactive/${id}`, 'PUT')
    .then((response) => {
      response.json();
      if (response.ok) {
        CallToastContainer(`product #${id} is now inactive`, 5000, true, toast.POSITION.TOP_CENTER, toast.success);
      }
    })
    .catch(() => {
      CallToastContainer('Unable to connect to database, Please try again later.', 3000, true, toast.POSITION.TOP_CENTER, toast.error);
      return false;
    });
}
