import React from 'react';
import styles from './DeleteProductButton.module.css';
/**
 * Modal that shows up every time you want to delete a product that hasent been purchased
 * @param {ObjArr} props hold functions setDisplayConfirmDeleteProductModal() and deleteProduct()
 * @returns a modal with a button to delete the product or to close the modal
 */
const ConfirmDeleteProductModal = ({ closeModal, deleteProduct }) => { //eslint-disable-line
  return (
    <div className={styles.overlay}>
      <div className={styles.modalBackground}>
        <div className="modalContainer">
          <h2 className={styles.center}>YOU ARE TRYING TO DELETE A PRODUCT!!!</h2>
          <p className={styles.center}>ARE YOU SURE YOU WANT TO DELETE THIS PRODUCT???</p>
          <button type="button" className={styles.modalBackButton} onClick={closeModal}>NO</button>
          <button type="button" className={styles.modalConfirmButton} onClick={deleteProduct}>YES</button>
        </div>
      </div>
    </div>
  );
};

export default ConfirmDeleteProductModal;
