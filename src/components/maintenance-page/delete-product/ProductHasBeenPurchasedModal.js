import React from 'react';
import styles from './DeleteProductButton.module.css';
/**
 * Modal that displays when a product has been purchased
 * @param {ObjArray} props hold the handelProductHasBeenPurchasedModal()
 * and the makeProductsStatusInactive() functions
 * @returns Modal covering the screen a button to close the modal and to set
 * a products status to inactive
 */
const ProductHasBeenPurchasedModal = ({ closeModal, makeProductStatusInactive }) => { //eslint-disable-line
  return (
    <div className={styles.overlay}>
      <div className={styles.modalBackground}>
        <div className="modalContainer">
          <h2 className={styles.center}>THIS PRODUCT HAS BEEN PART OF A PREVIOUS ORDER!!!</h2>
          <p className={styles.center}>WOULD YOU LIKE TO MARK THIS PRODUCT AS INACTIVE INSTEAD</p>
          <button type="button" className={styles.modalBackButton} onClick={closeModal}>NO</button>
          <button type="button" className={styles.modalConfirmButton} onClick={makeProductStatusInactive}>YES</button>
        </div>
      </div>
    </div>
  );
};

export default ProductHasBeenPurchasedModal;
