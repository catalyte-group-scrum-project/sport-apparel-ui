import React from 'react';
import { Tooltip } from '@material-ui/core';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import { useCart } from './CartContext';
import OrderItem from './OrderItem';
import {
  getSubtotal, getShippingPrice, getTotalPrice, toPrice
} from './ReviewOrderWidgetService';
import styles from './ReviewOrderWidget.module.css';
/* eslint-disable react/destructuring-assignment */

/**
 * @name ReviewOrderWidget
 * @param {Object[]} deliveryState
 * @description Displays order items, subtotal, and the shipping information.
 * @return component
 */
const ReviewOrderWidget = (deliveryState) => {
  const {
    state: { products }
  } = useCart();

  const [deliveryPrice, setDeliveryPrice] = React.useState();

  React.useEffect(() => {
    setDeliveryPrice(getShippingPrice(products, deliveryState.deliveryState));
  }, [products, deliveryState]);

  return (
    <>
      {products.map(({
        price, title, description, quantity, imageSrc
      }) => (
        <OrderItem
          key={title}
          price={price}
          title={title}
          description={description}
          quantity={quantity}
          imageSrc={imageSrc}
        />
      ))}
      <hr />
      <div className={styles.subtotal}>
        <div>
          <p>Subtotal</p>
        </div>
        <div className={styles.price}>
          <p>{getSubtotal(products)}</p>
        </div>
        <div>
          Shipping Price
        </div>
        <div className={styles.shippingPrice}>
          <div>
            $
            {deliveryPrice}
            .00
            <Tooltip
              title={
                (
                  <ul>
                    <h2>Shipping +$5.00 on orders less than 50$</h2>
                    <h2>Shipping +$10.00 on orders to Alaska or Hawaii</h2>
                  </ul>
                )
              }
            >
              <HelpOutlineIcon
                fontSize="small"
              />
            </Tooltip>
          </div>
        </div>
        <div>
          <p>Total Price</p>
        </div>
        <div className={styles.totalPrice}>
          <p>
            {toPrice(getTotalPrice(deliveryPrice, products))}
          </p>
        </div>
      </div>
    </>
  );
};

export default ReviewOrderWidget;
