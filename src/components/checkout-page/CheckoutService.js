import { toast } from 'react-toastify';
import HttpHelper from '../../utils/HttpHelper';
import Constants from '../../utils/constants';
import CallToastContainer from '../toast/toast';

/**
 *
 * @name makePurchase
 * @description sends a purchase request
 * @param {*} cartContents items to purchase
 * @returns payment confirmation response
 */
// eslint-disable-next-line max-len
export default async function makePurchase(deliveryAddress, billingAddress, creditCard, lineItems, purchasePrice, e) {
  await HttpHelper(Constants.PURCHASE_ENDPOINT, 'POST', {
    deliveryAddress,
    billingAddress,
    creditCard,
    lineItems,
    purchasePrice
  })
    .then((response) => response.json())
    .catch(() => {
      // generate toast if the server can't be reached
      // eslint-disable-next-line quotes
      CallToastContainer(`We're sorry, we were unable to reach the server at this time.`, 8000, true, toast.POSITION.TOP_CENTER, toast.error);

      // prevent the confirmation page from loading if the server can't be reached
      e.preventDefault();
    });
}
