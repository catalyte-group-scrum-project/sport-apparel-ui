import React from 'react';
import { updateLastActiveTime } from '../header/HeaderService';

const CartContext = React.createContext();

function cartReducer(state, action) {
  switch (action.type) {
    case 'delete': {
      return {
        ...state,
        products: state.products.filter((product) => product.title !== action.product.title)
      };
    }
    case 'add': {
      if (state.products.length === 0) {
        return {
          ...state,
          products: [...state.products, action.product]
        };
      }
      const idArray = [];
      state.products.forEach((product) => {
        idArray.push(product.id);
      });
      if (idArray.includes(action.product.id)) {
        return {
          ...state,
          products: state.products.map((product) => (product.id === action.product.id
            ? { ...product, quantity: product.quantity + action.product.quantity }
            : product))
        };
      }
      return {
        ...state,
        products: [...state.products, action.product]
      };
    }
    case 'empty_cart': {
      return {
        ...state,
        products: state.products.splice(0, state.products.length)
      };
    }
    case 'toggle-liked': {
      return {
        ...state,
        areProductsLiked: true
      };
    }
    case 'toggle-disliked': {
      return {
        ...state,
        areProductsLiked: false
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

/**
 * Helper function that gets cart from local storage on function call
 * @returns products in cart from local storage
 */
const cartFromLocalStorage = () => JSON.parse(localStorage.getItem('cart') || '[]');

function CartProvider({ children }) {
  const initialProducts = {
    products: cartFromLocalStorage(),
    areProductsLiked: false,
    setProducts: () => { }
  };
  const [state, dispatch] = React.useReducer(cartReducer, initialProducts);

  const value = { state, dispatch };
  const LoggedIn = JSON.parse(sessionStorage.getItem('loggedIn'));
  if (LoggedIn) { updateLastActiveTime(); }

  return (
    <CartContext.Provider value={value}>
      {children}
    </CartContext.Provider>
  );
}

function useCart() {
  const context = React.useContext(CartContext);
  if (context === undefined) {
    throw new Error('useCartDispatch must be used within a CartProvider');
  }
  return context;
}

export { CartProvider, useCart, cartFromLocalStorage };
