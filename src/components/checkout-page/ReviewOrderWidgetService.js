/**
 * converts a price to a formatted string
 * @param {number} price
 * @returns {string} formatted price
 */
export const toPrice = (price) => `$${price.toFixed(2)}`;

export const getTotal = (products) => products.reduce(
  (acc, item) => acc + (item.quantity * item.price), 0
);
/**
 * Gets the subtotal of an order
 * @param {Object []} products
 * @returns Number
 */
export const getSubtotal = (products) => {
  if (products.length > 0) {
    return toPrice(products.reduce(
      (acc, item) => acc + (item.quantity * item.price), 0
    ));
  }
  return '0.00';
};
/**
 * A function that takes in the products and the delivery state and calculates the shipping cost
 * @param {Object[]} products
 * @param {Object[]} deliveryState
 * @returns the price of shipping
 */
export const getShippingPrice = (products, deliveryState) => {
  let shippingTotal = 5;
  const total = getTotal(products);
  const expensiveState = ['Alaska', 'Hawaii', 'Guam', 'Federated States of Micronesia', 'Marshall Islands', 'Northern Mariana Islands', 'Virgin Island', 'Palau', 'Puerto Rico', 'American Samoa'];
  if (expensiveState.includes(deliveryState)) {
    shippingTotal += 5;
  }
  if (total < 100 && getTotal(products) > 50) {
    shippingTotal -= 5;
  }
  if (total > 100) {
    shippingTotal -= 10;
  }
  if (shippingTotal < 0) {
    shippingTotal = 0;
  }
  return shippingTotal;
};

/**
 * Determins wheather the total is within the range for a shipping discount
 * @param {Object[]} products
 * @param {Object[]} deliveryState
 * @returns true if total is between $45.00 and $50.00 or between 95 and 100.
 */
export const validForShippingDiscount = (products) => {
  const totalForShippngDiscount = (getTotal(products));
  if ((totalForShippngDiscount >= 45.00 && totalForShippngDiscount < 50.00) || (totalForShippngDiscount >= 95.00 && totalForShippngDiscount < 100.00)) { //eslint-disable-line
    return true;
  }
  return false;
};

// eslint-disable-next-line max-len
export const getTotalPrice = (deliveryPrice, products) => deliveryPrice + getTotal(products);
