import React from 'react';
import styles from './ShippingSavingsModal.module.css';
/* eslint-disable react/destructuring-assignment */
/**
 * takes in handleCheckoutModal which is a object with the handleCheckoutModal
 * function and the handlePay function
 * @param {Object[]} handleCheckoutModal
 * @returns
 */
const ShippingSavingsModal = (handleCheckoutModal) => { //eslint-disable-line
  return (
    <div className={styles.overlay}>
      <div className={styles.checkoutModalBackground}>
        <div className="modalContainer">
          <h3 className={styles.center}>SAVE YOURSELF SOME MONEY!!!</h3>
          <p className={styles.center}>Purchases above $50 get $5 off shipping!!!</p>
          <p className={styles.center}>Purchases above $100 get free shipping!!!</p>
          <button type="button" className={styles.checkoutModalBackButton} onClick={handleCheckoutModal.handleCheckoutModal}>Back</button>
          <button type="button" className={styles.checkoutModalCheckoutButton} onClick={handleCheckoutModal.handlePay}>Checkout</button>
        </div>
      </div>
    </div>
  );
};
export default ShippingSavingsModal;
