/* eslint-disable max-len */
import React from 'react';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { useCart } from './CartContext';
import styles from './CheckoutPage.module.css';
import ReviewOrderWidget from './ReviewOrderWidget';
import DeliveryAddress from './forms/DeliveryAddress';
import BillingDetails from './forms/BillingDetails';
import makePurchase from './CheckoutService';
import ValidateDataObject from '../../utils/Validation';
import ShippingSavingsModal from './ShippingSavingsModal';
import { validForShippingDiscount, getShippingPrice, getTotalPrice } from './ReviewOrderWidgetService';
import CallToastContainer from '../toast/toast';
import { updateLastActiveTime } from '../header/HeaderService';

/**
 * @name CheckoutPage
 * @description A view that contains details needed to process a transaction for items
 * @return component
 */
const CheckoutPage = () => {
  const history = useHistory();

  const {
    state: { products }
  } = useCart();

  const [billingData, setBillingData] = React.useState({});

  const onBillingChange = (e) => {
    setBillingData({ ...billingData, [e.target.id]: e.target.value });
  };

  const [deliveryData, setDeliveryData] = React.useState({});

  const onDeliveryChange = (e) => {
    setDeliveryData({ ...deliveryData, [e.target.id]: e.target.value });
  };

  const [checked, setChecked] = React.useState(false);

  const handleCheck = () => {
    setChecked(!checked);
  };
  const [openCheckoutModal, setOpenCheckoutModal] = React.useState(false);

  const [checkoutErrors, setCheckoutErrors] = React.useState({});

  const handleCheckoutModal = () => {
    setOpenCheckoutModal(!openCheckoutModal);
  };
  const handlePay = (e) => {
    const productData = products.map(({ id: productId, quantity }) => ({ productId, quantity }));
    const deliveryAddress = {
      deliveryFirstName: deliveryData.firstName,
      deliveryLastName: deliveryData.lastName,
      deliveryStreet: deliveryData.street,
      deliveryStreet2: deliveryData.street2,
      deliveryCity: deliveryData.city,
      deliveryState: deliveryData.state,
      deliveryZip: deliveryData.zip,
      shippingCost: getShippingPrice(products, deliveryData.state)
    };
    const billingAddress = {};
    if (checked) {
      billingAddress.billingStreet = deliveryAddress.deliveryStreet;
      billingAddress.billingStreet2 = deliveryAddress.deliveryStreet2;
      billingAddress.billingCity = deliveryAddress.deliveryCity;
      billingAddress.billingState = deliveryAddress.deliveryState;
      billingAddress.billingZip = deliveryAddress.deliveryZip;
    } else {
      billingAddress.billingStreet = billingData.billingStreet;
      billingAddress.billingStreet2 = billingData.billingStreet2;
      billingAddress.billingCity = billingData.billingCity;
      billingAddress.billingState = billingData.billingState;
      billingAddress.billingZip = billingData.billingZip;
    }
    billingAddress.email = billingData.email;
    billingAddress.phone = billingData.phone;

    const creditCard = {
      cardNumber: billingData.creditCard,
      cvv: billingData.cvv,
      expiration: billingData.expiration,
      cardholder: billingData.cardholder
    };

    const purchasePrice = parseFloat(getTotalPrice(deliveryAddress.shippingCost, products).toFixed(2));

    // perform validation
    if (!ValidateDataObject(billingData, deliveryAddress, setCheckoutErrors, checked)) {
      CallToastContainer('The transaction could not be processed. You will not be charged.', 8000, true, toast.POSITION.TOP_CENTER, toast.error);
    } else {
      // reset error messages if validation passes
      setCheckoutErrors({
        creditCard: '',
        cvv: '',
        expiration: '',
        cardholder: '',
        billingStreet: '',
        billingCity: '',
        billingState: '',
        billingZip: '',
        email: '',
        firstName: '',
        lastName: '',
        deliveryStreet: '',
        deliveryCity: '',
        deliveryState: '',
        deliveryZip: ''
      });
    }

    // generate toast if the cart is empty when checkout is clicked
    if (products.length === 0) {
      CallToastContainer('There must be one or more items in the cart to make a purchase', 8000, true, toast.POSITION.TOP_CENTER, toast.warning);
      e.preventDefault();
    // if all validation passes and the cart has products, make the purchase
    } else if (ValidateDataObject(billingData, deliveryAddress, setCheckoutErrors, checked)) {
      localStorage.setItem('cart', '[]');
      makePurchase(deliveryAddress, billingAddress, creditCard, productData, purchasePrice).then(() => history.push('/confirmation'));
      const LoggedIn = JSON.parse(sessionStorage.getItem('loggedIn'));
      if (LoggedIn) { updateLastActiveTime(); }
    }
  };

  return (
    <div className={styles.checkoutContainer}>
      <CallToastContainer />
      <div className={`${styles.step} ${styles.order}`}>
        <h3 className={styles.title}>1. Review Order</h3>
        <ReviewOrderWidget deliveryState={deliveryData.state} />
      </div>
      <div className={`${styles.step} ${styles.delivery}`}>
        <h3 className={styles.title}>2. Delivery Address</h3>
        <DeliveryAddress
          onChange={onDeliveryChange}
          deliveryData={deliveryData}
          checkoutErrors={checkoutErrors}
        />
        <label htmlFor="useSame" className={styles.sameAddressText}>
          <div className={styles.useSameAddress}>
            <input
              id="useSame"
              onChange={handleCheck}
              type="checkbox"
              value={checked}
            />
          </div>
          Same Billing Address
        </label>
      </div>
      <div className={`${styles.step} ${styles.payment}`}>
        <h3 className={styles.title}>3. Billing Details</h3>
        <BillingDetails
          onChange={onBillingChange}
          billingData={billingData}
          useShippingForBilling={checked}
          checkoutErrors={checkoutErrors}
        />
      </div>
      {openCheckoutModal
        ? (
          <ShippingSavingsModal handleCheckoutModal={handleCheckoutModal} handlePay={handlePay} />
        ) : <div />}
      <div className={styles.payNow}>
        {validForShippingDiscount(products) ? <button onClick={handleCheckoutModal} type="button" className={styles.shippingDiscountButton}>Checkout</button>
          : <button onClick={handlePay} type="button" className={styles.payButton}>Checkout</button>}
      </div>
    </div>
  );
};
export default CheckoutPage;
