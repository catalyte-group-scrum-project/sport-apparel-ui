import React from 'react';
import { Link } from 'react-router-dom';
import PopularProductsWidget from './popular-products-widget';
import styles from '../checkout-page/CheckoutPage.module.css';

const HomePage = () => (
  <div className={styles.popularProductsContainer}>
    <div className={styles.stepPopularProducts}>
      <h3 className={styles.titlePopularProducts}>Popular Products!</h3>
      <div className={styles.buttonLocationPopularProducts}>
        <Link to="/products"><button type="button" className={styles.popularProductsButton}>All Products</button></Link>
      </div>
      <div className={styles.popularProducts}>
        <PopularProductsWidget />
      </div>
    </div>
  </div>
);

export default HomePage;
