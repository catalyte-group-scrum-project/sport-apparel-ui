import React, { useState, useEffect } from 'react';
import styles from '../product-page/ProductPage.module.css';
import GetPopularProducts from './home-page-service';
import ProductCard from '../product-card/ProductCard';
import Constants from '../../utils/constants';

const PopularProductsWidget = () => {
  const [apiError, setApiError] = useState(false);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    GetPopularProducts(setProducts, setApiError);
  }, []);

  return (
    <div>
      {apiError && <p className={styles.errMsg} data-testid="errMsg">{Constants.API_ERROR}</p>}
      <div className={styles.appPopularProducts}>
        {products.map((product) => (
          <div key={product.id}>
            <ProductCard
              product={product}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default PopularProductsWidget;
