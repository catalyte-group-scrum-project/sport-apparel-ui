/* eslint-disable no-console */
import React, { useEffect, useState, useContext } from 'react';
import { MdFilterAlt, MdArrowBack } from 'react-icons/md';
import { toast } from 'react-toastify';
import ReactTooltip from 'react-tooltip';
import { useLocation } from 'react-router-dom';
import SidebarData from '../product-filter/FilterData';
import ProductCard from '../product-card/ProductCard';
import styles from './ProductPage.module.css';
import Constants from '../../utils/constants';
import { ProductContext } from './ProductContext';
import {
  updateLatestView, updateProductViews,
  fetchUserEmail, getReviewsByEmail
} from './ProductPageService';
import ModalContainer from '../productModal/ModalContainer';
import { useCart } from '../checkout-page/CartContext';
import parseProductsFromPurchases from './userProducts';
import { getPurchaseHistory } from '../account-page/purchase-history/PurchaseHistoryService';
// import asyncFetchUserProducts from './userProducts';
// import { getUserEmail } from '../header/HeaderService';
import { addToWishList, getLikedProductIds } from '../account-page/wishlist/WishListService';
import CallToastContainer from '../toast/toast';

/**
 * @name ProductPage
 * @description fetches products from API and displays products as product cards
 * @param {boolean} loginStatus boolen true if logged in
 * @param {boolean} navToPage boolen true when we first navigate to the page
 * @return component
 */
const ProductPage = ({ loginStatus }) => {
  // sets default page number
  const {
    currentPage,
    setCurrentPage,
    maxNumber,
    apiError,
    filteredProducts,
    filterSelections,
    searchedTerm,
    setApiError
  } = useContext(ProductContext);
  const [likedProductIds, setLikedProductIds] = useState([]);
  const [toggleEffect, setToggleEffect] = useState(false);
  const [userEmail, setUserEmail] = useState([null]);
  const [userPurchaseHistory, setUserPurchaseHistory] = useState([null]);
  const [userProductsReviewed, setUserProductsReviews] = useState([null]);
  const [userProductsPurchased, setUserProductsPurchased] = useState([null]);
  const location = useLocation(); // when navigating to this page this updates
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const handleToggle = () => setSidebarOpen(!sidebarOpen);
  // gets filtered product from the backend based on used selections, as well as the liked products
  useEffect(() => {
    fetchUserEmail(setUserEmail, loginStatus);
    getLikedProductIds(setLikedProductIds);
  }, [loginStatus, toggleEffect]);
  /* const checkUserProducts = () => {
    fetchUserEmail(setUserEmail);
  }; */
  useEffect(() => {
    const validEmailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (validEmailRegEx.test(userEmail)) { /* if there is a valid email */
      getPurchaseHistory(setUserPurchaseHistory, userEmail, setApiError);
      getReviewsByEmail(setUserProductsReviews, userEmail);
    } else { /* otherwise reset all user data */
      setUserPurchaseHistory(null);
      setUserProductsReviews(null);
      setUserProductsPurchased(null);
    }
  }, [userEmail, location, setApiError]); /* wait until the user's email addr is returned */
  useEffect(() => {
    if (userPurchaseHistory != null) {
      parseProductsFromPurchases(
        setUserProductsPurchased,
        userPurchaseHistory
      );
    }
  }, [userPurchaseHistory]); /* wait until the history is returned */

  // sets page option range
  const maxPage = (Math.ceil(maxNumber / 20));
  // set start and end page for page selector
  let startPage;
  let endPage;
  if (maxPage <= 10) {
    // less than 10 total pages so show all
    startPage = 1;
    endPage = maxPage;
  } else if (currentPage <= 6) {
    // more than 10 total pages so calculate start and end pages
    startPage = 1; endPage = 10;
  } else if (currentPage + 4 >= maxPage) {
    startPage = maxPage - 9;
    endPage = maxPage;
  } else {
    startPage = currentPage - 5;
    endPage = currentPage + 4;
  }

  const pages = [...Array((endPage + 1) - startPage).keys()].map((i) => (startPage + i));

  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [modalProduct, setModalProduct] = useState({
    description: 'initialized',
    demographic: 'Kids',
    price: 0.00,
    title: 'Name',
    quantity: 1,
    type: 'Sport',
    category: 'initialized',
    primaryColorCode: '0x000000',
    secondaryColorCode: '0x000000'
  });
  const closeModal = () => {
    setModalIsOpen(false);
  };
  let currentTime = 0;
  const handelTime = () => {
    currentTime = new Date().toUTCString();
    return currentTime;
  };
  const openModal = ({ product }) => {
    handelTime();
    if (product.latestView !== currentTime && (product.latestView + 60000) <= currentTime) {
      updateProductViews(product.id);
      updateLatestView(product.id);
    }
    setModalIsOpen(true);
    if (product != null) {
      setModalProduct(product);
    }
  };
  const checkKeys = (e) => {
    if (e.key === 'Escape') closeModal();
  };

  const {
    state: { products }
  } = useCart();

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(products));
  }, [products]);
  const { dispatch } = useCart();
  const displayLikedButton = () => {
    dispatch(
      {
        type: 'toggle-liked'
      }
    );
  };
  const handelLikeProduct = (product) => {
    if (likedProductIds.includes(product.id)) {
      CallToastContainer('Product is already part of your wishlist', 3000, true, toast.POSITION.TOP_CENTER, toast.error);
    } else {
      addToWishList(product, displayLikedButton);
      setToggleEffect(!toggleEffect);
    }
  };
  return (
    <div>
      {apiError && <p className={styles.errMsg} data-testid="errMsg">{Constants.API_ERROR}</p>}
      <div className={styles.filterButtonContainer}>
        <button className={styles.filterButton} type="button" onClick={handleToggle} data-tip data-for="filterTip">
          <MdFilterAlt MdCode size="2em" color="white" />
        </button>
        {searchedTerm !== '' && (
        <h1 className={styles.searchedTerm}>
          Search Results For
          {' '}
          `
          {searchedTerm}
          ` matched
          {' '}
            {maxNumber}
          {' '}
          Result
        </h1>
        )}
      </div>
      <div className={sidebarOpen ? styles.sidebarOpen : styles.sidebar}>
        <MdArrowBack MdCode size="2em" color="cornflowerblue" onClick={handleToggle} className={styles.closeButton} />
        <h1 className={styles.title}>Filter By</h1>
        <SidebarData filterSelections={filterSelections} />
      </div>
      <div className={sidebarOpen ? styles.container : styles.normal}>
        {maxNumber === 0
          ? (
            <h1 className={styles.noProduct}>
              There are no products meeting your criteria.
            </h1>
          ) : (
            <div className={styles.app}>
              {filteredProducts.map((product) => (
                <div key={product.id}>
                  <ProductCard
                    product={product}
                    toggleModal={openModal}
                    modalIsOpen={modalIsOpen}
                    likedProductIds={likedProductIds}
                    handelLikeProduct={handelLikeProduct}
                  />
                </div>
              ))}
            </div>
          )}
      </div>

      {modalIsOpen && (
        <ModalContainer
          modalIsOpen={modalIsOpen}
          toggleModal={closeModal}
          modalProduct={modalProduct}
          onKeyDown={checkKeys}
          userProducts={userProductsPurchased}
          userReviews={userProductsReviewed}
          setUserProductsReviews={setUserProductsReviews}
          userEmail={userEmail}
          likedProductIds={likedProductIds}
          handelLikeProduct={handelLikeProduct}
          email={userEmail}
        />
      )}
      <ReactTooltip id="filterTip" place="right" effect="solid" delayShow={800} backgroundColor="grey" z-index="1">
        Filter Products
      </ReactTooltip>

      <div className={styles.spacing} />

      <footer className={styles.navigation}>
        <div className={styles.paginationContainer} hidden={maxPage === 1 || maxPage === 0}>
          <ul className={styles.pagination}>
            <li className={styles.paginationLi}>
              <div>
                <button className={styles.first} type="button" onClick={() => setCurrentPage(1)} disabled={currentPage === 1}>
                  First
                </button>
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                <button className={styles.prev} type="button" onClick={() => setCurrentPage(currentPage - 1)} disabled={currentPage === 1}>
                  Previous
                </button>
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                {pages.map((pageNumber) => (
                  <button className={styles.pageSelector} type="button" onClick={() => setCurrentPage(pageNumber)} disabled={currentPage === pageNumber}>
                    {pageNumber}
                  </button>
                ))}
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                <button className={styles.next} type="button" onClick={() => setCurrentPage(currentPage + 1)} disabled={currentPage === maxPage}>
                  Next
                </button>
              </div>
            </li>

            <li className={styles.paginationLi}>
              <div>
                <button className={styles.last} type="button" onClick={() => setCurrentPage(maxPage)} disabled={currentPage === maxPage}>
                  Last
                </button>
              </div>
            </li>
          </ul>
        </div>
      </footer>
    </div>
  );
};

export default ProductPage;
//
