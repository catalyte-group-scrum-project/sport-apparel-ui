import HttpHelper from '../../utils/HttpHelper';
import Constants from '../../utils/constants';

/**
 *
 * @name fetchProducts
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function fetchProducts(setProducts, setApiError) {
  await HttpHelper(Constants.ALL_PRODUCTS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchProducts
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function fetchActiveProducts(setProducts, setApiError) {
  await HttpHelper(Constants.ACTIVE_PRODUCTS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchProductCategories
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for product attributes if 200 response, else sets state for apiError
 */
export async function fetchProductCategories(setProducts, setApiError) {
  await HttpHelper(Constants.PRODUCT_CATEGORIES_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchProductBrands
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for product attributes if 200 response, else sets state for apiError
 */
export async function fetchProductBrands(setProducts, setApiError) {
  await HttpHelper(Constants.PRODUCT_BRANDS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchProductDemographics
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for product attributes if 200 response, else sets state for apiError
 */
export async function fetchProductDemographics(setProducts, setApiError) {
  await HttpHelper(Constants.PRODUCT_DEMOGRAPHICS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchProductPrimaryColorCode
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for product attributes if 200 response, else sets state for apiError
 */
export async function fetchProductPrimaryColorCode(setProducts, setApiError) {
  await HttpHelper(Constants.PRODUCT_PRIMARY_COLORS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchProductMaterials
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for products attributes if 200 response, else sets state for apiError
 */
export async function fetchProductMaterials(setProducts, setApiError) {
  await HttpHelper(Constants.PRODUCT_MATERIALS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchFilteredProducts
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @param {*} filterData user selected filter options used to retrieve products based on selections
 * @param {*} pageNumber current page number for pagination
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function fetchFilteredProducts(setProducts, setApiError, filterData, pageNumber, searchTerm = '') {
  await HttpHelper(`/products/filter/?pageNumber=${pageNumber}&pageSize=20&${filterData}&searchTerm=${searchTerm}`, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name fetchFilteredProductsMaxNumber
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setMaxNumber sets max product number
 * @param {*} setApiError sets error if response other than 200 is returned
 * @param {*} filterData user selected filter options used to retrieve products based on selections
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function fetchFilteredProductsMaxNumber(setApiError, filterData, searchTerm = '', setMaxNumber) {
  await HttpHelper(`/products/maxNumber/?${filterData}&searchTerm=${searchTerm}`, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setMaxNumber)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name updateProductViews
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function updateProductViews(id, setProducts, setApiError) {
  await HttpHelper(`/products/update/productViews/${id}`, 'PUT')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name updateLatestView
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setProducts sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function updateLatestView(id, setProducts, setApiError) {
  await HttpHelper(`/products/update/latestView/${id}`, 'PUT')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setProducts)
    .catch(() => {
      setApiError(true);
    });
}
/* @name fetchUserEmail
 * @description grap user info after user is logged in
 * @returns User email
 */
export async function fetchUserEmail(setUserEmail, loginStatus) {
  if (loginStatus === true) {
    const email = await fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${sessionStorage.getItem('token')}`)
      .then((res) => res.json())
      .then((user) => (user.email));
    return (setUserEmail(email));
  // eslint-disable-next-line no-else-return
  } else {
    return (setUserEmail(null));
  }
}
/**
 * @name getReviewsByEmail
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setReviewdProducts sets state for which products hacve reviews
 * @param {*} email the user whose reviews we will fetch
 * @returns sets state for products if 200 response, else sets state for apiError
 */
export async function getReviewsByEmail(setReviewedProducts, email) {
  const userReviewEndpoint = `${Constants.ALL_REVIEWS_ENDPOINT}/${email}`;
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (re.test(email)) { /* regex test that email is a vailid email */
    await HttpHelper(userReviewEndpoint, 'GET')
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('getReviews is working incorrectly');
      })
      .then((body) => {
        const idsOfProductsWithReviews = [];
        body.map((review) => idsOfProductsWithReviews.push(review.productId));
        setReviewedProducts(idsOfProductsWithReviews);
      });
  } else { // if we don't have a valid email, reset products to empty
    setReviewedProducts(null);
  }
}
