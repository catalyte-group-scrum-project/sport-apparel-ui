// import { useState } from 'react';

// import { getUserEmail } from '../header/HeaderService';
// import Constants from '../../../utils/constants';

/**
 * @name parseProductsFromPurchases
 * @description creates an array of productIds that the user has purchased
 * @param setUserProductsPurchased changes the state of userProductsPurchased
 * @param purchases purchase items retrieved from the API
 */
const parseProductsFromPurchases = (
  setUserProductsPurchased,
  purchases
) => {
  // eslint-disable-next-line prefer-const
  let productIds = [];
  let i = 0;
  if (purchases.length > 0) {
    purchases.forEach((purchase) => {
      if (purchase != null) {
        purchase.lineItems.forEach((lineItem) => {
          productIds[i] = lineItem.productId;
          i += 1;
        });
      }
    });
    setUserProductsPurchased(productIds);
  }
};

export default parseProductsFromPurchases;
