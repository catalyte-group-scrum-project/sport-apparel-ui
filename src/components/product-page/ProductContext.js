import React, { createContext, useState, useEffect } from 'react';
import { fetchFilteredProducts, fetchFilteredProductsMaxNumber } from './ProductPageService';

export const ProductContext = createContext();

export default function ProductProvider({ children }) {
  const [currentPage, setCurrentPage] = useState(1);
  const [maxNumber, setMaxNumber] = useState(Number);
  const [apiError, setApiError] = useState(false);
  const [filteredProducts, setFilteredProducts] = useState([]);
  // filter data being sent from product-filter
  const [filterData, setFilterData] = useState('');
  const [searchedTerm, setSearchedTerm] = useState('');
  const filterSelections = (dataFromFilter) => {
    setFilterData(dataFromFilter);
  };

  // gets filtered product from the backend based on used selections
  useEffect(() => {
    fetchFilteredProducts(
      setFilteredProducts,
      setApiError, filterData, currentPage, searchedTerm?.toLowerCase().slice().replace(/\s{2,}/g, ' ').replace(/,/g, ' ')
        .trim()
    );
  }, [filterData, currentPage, searchedTerm]);
  // sets page option range

  useEffect(() => {
    fetchFilteredProductsMaxNumber(setApiError, filterData, searchedTerm?.toLowerCase().slice().replace(/\s{2,}/g, ' ').replace(/,/g, ' ')
      .trim(), setMaxNumber);
  }, [filterData, searchedTerm]);

  const value = {
    currentPage,
    setCurrentPage,
    maxNumber,
    setMaxNumber,
    apiError,
    setApiError,
    filteredProducts,
    setFilteredProducts,
    filterData,
    setFilterData,
    searchedTerm,
    setSearchedTerm,
    filterSelections
  };

  return (
    <ProductContext.Provider value={value}>
      {children}
    </ProductContext.Provider>
  );
}
