export default Object.freeze({
  API_ERROR: 'Oops, something went wrong',
  BASE_URL_API: 'http://localhost:8085',
  PLACEHOLDER_IMAGE: 'https://www.signfix.com.au/wp-content/uploads/2017/09/placeholder-600x400.png',
  PURCHASE_ENDPOINT: '/purchases',
  GOOGLE_CLIENT_ID: '312186882431-ubld205po8lfd1sjjlt2erdpc2sk4o97.apps.googleusercontent.com', // ENTER CLIENT ID HERE
  ACTIVE_PRODUCTS_ENDPOINT: '/products/status/active',
  ALL_PRODUCTS_ENDPOINT: '/products',
  ALL_REVIEWS_ENDPOINT: '/reviews',
  PRODUCT_CATEGORIES_ENDPOINT: '/products/categories',
  PRODUCT_BRANDS_ENDPOINT: '/products/brands',
  PRODUCT_DEMOGRAPHICS_ENDPOINT: '/products/demographics',
  PRODUCT_PRIMARY_COLORS_ENDPOINT: '/products/primarycolor',
  PRODUCT_MATERIALS_ENDPOINT: '/products/material',
  PRODUCT_FILTERS_ENDPOINT: '/products/filter',
  PRODUCT_MAXNUMBER_ENDPOINT: '/products/maxNumber',
  PROMO_CODE_ENDPOINT: '/promocodes'
});
